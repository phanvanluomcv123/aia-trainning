using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Contracts.Repository;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.Models;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AIA.One.Core.CQRS.Models;
using AIA.One.Core.Utils;
using AutoMapper;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AIA.Training.Core.Configs;
using AIA.One.Core;

namespace AIA.Training.Business
{
    public partial class CategoryBusiness : ICategoryBusiness
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public CategoryBusiness(ICategoryRepository repository, IMapper mapper, ILogger logger) : this(repository)
        {
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ListResult<GetCategoryResult>> GetAll(GetCategories query)
        {
            Expression<Func<Category, bool>> condition = x => true;
            if (!string.IsNullOrEmpty(query.Name))
            {
                condition = condition.And(x => x.Name.Contains(query.Name));
            }

            if (query.Deleted != null)
            {
                condition = condition.And(x => x.Deleted == query.Deleted);
            }

            if (query.PageIndex == 0)
            {
                query.PageIndex = Constants.DEFAULT_PAGE_INDEX;
            }

            if (query.PageSize == 0)
            {
                query.PageSize = Constants.DEFAULT_PAGE_SIZE;
            }

            if (query.OrderBy == null || query.OrderBy.Count == 0)
            {
                query.OrderBy = new List<OrderBy>
                {
                    new OrderBy
                    {
                        PropertyName = nameof(Category.CreatedDateTime),
                        Desc = true
                    }
                };
            }
            var data = repository
                    .List(condition, query.OrderBy, query.PageIndex, query.PageSize, out var total).ToList();

            var result = new ListResult<GetCategoryResult>
            {
                Items = _mapper.Map<List<GetCategoryResult>>(data),
                TotalRecords = total
            };

            return await Task.FromResult(result);
        }

        public async Task<GetCategoryResult> GetDetail(GetCategory query)
        {
            var courseType = repository.List(q => q.Id == query.Id).FirstOrDefault();
            if (courseType != null)
            {
                return await Task.FromResult(_mapper.Map<GetCategoryResult>(courseType));
            }
            return null;
        }

        public async Task CreateOrUpdate(CreateOrUpdateCategory command)
        {
            if (string.IsNullOrEmpty(command.Id))
            {
                var Category = new Category()
                {
                    Name = command.Name,
                    Description = command.Description,
                    Deleted = command.Deleted
                };

                repository.Insert(Category, true);
            }
            else
            {
                var entity = repository.GetById(command.Id);
                if (entity == null)
                {
                    throw new NullReferenceException();
                }

                entity.Name = command.Name;
                entity.Deleted = command.Deleted;
                entity.Description = command.Description;
                entity.LastUpdatedBy = command.Username;
                entity.LastUpdatedDateTime = DateTime.UtcNow;

                repository.Update(entity, true);
            }

            await Task.CompletedTask;
        }

        public bool IsCategoryNameExist(string name, string exceptCategoryId = null)
        {
            return string.IsNullOrEmpty(exceptCategoryId)
                ? repository.Any(_ => _.Name == name)
                : repository.Any(_ => _.Name == name && _.Id != exceptCategoryId);
        }
    }
}
