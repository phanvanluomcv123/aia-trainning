﻿using AIA.Training.Core.Contracts.Business;
using AIA.One.Core.DependencyInjection;
using System;

namespace AIA.Training.Business
{
    public class Bootstrapper : IWireUpDependencies
    {
	    protected readonly IRegisterDependencies registerDependencies;

        public Bootstrapper(IRegisterDependencies registerDependencies)
        {
            if (registerDependencies == null)
            {
                throw new ArgumentNullException("registerDependencies");
            }

            this.registerDependencies = registerDependencies;
        }

        public virtual void WireUp()
        {
            // register business
            this.registerDependencies.Register<ICategoryBusiness, CategoryBusiness>();
            this.registerDependencies.Register<IRoomBusiness, RoomBusiness>();
            this.registerDependencies.Register<IRoomServiceBusiness, RoomServiceBusiness>();
            this.registerDependencies.Register<IRoomStatusBusiness, RoomStatusBusiness>();
            this.registerDependencies.Register<IRoomTypeBusiness, RoomTypeBusiness>();
            this.registerDependencies.Register<IStudentBusiness, StudentBusiness>();
        }
	}
}
