using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Contracts.Repository;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.Models;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AIA.One.Core.CQRS.Models;
using AIA.One.Core.Utils;
using AutoMapper;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AIA.Training.Core.Configs;
using AIA.One.Core;

namespace AIA.Training.Business
{
    public partial class StudentBusiness : IStudentBusiness
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public StudentBusiness(IStudentRepository repository, IMapper mapper, ILogger logger) : this(repository)
        {
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ListResult<GetStudentResult>> GetAll(GetStudent query)
        {
            var result = new ListResult<GetStudentResult>();
            //todo => get data from database
            var data = repository.List().ToList();
            result.Items = _mapper.Map<List<GetStudentResult>>(data);
            return await Task.FromResult(result);
        }
    }
}
