using AIA.Training.Core.Configs;
using AIA.Training.Core.Contracts.Business;
using AIA.One.Core.AzureBlob;
using AIA.One.Core.DependencyInjection;
using AIA.Training.Core.Contracts.ExternalService;
using AIA.Training.Business.ExternalSerivce;

namespace AIA.Training.Business
{
    public class ManualBootstrapper : Bootstrapper
    {
        public ManualBootstrapper(IRegisterDependencies registerDependencies) : base(registerDependencies)
        {
        }

        public override void WireUp()
        {
            base.WireUp();

            // blob

            registerDependencies.Register<IDemoJob, DemoJob>();
        }
    }
}
