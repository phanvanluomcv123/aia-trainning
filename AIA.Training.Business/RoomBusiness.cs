﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AIA.One.Core;
using AIA.One.Core.CQRS.Models;
using AIA.One.Core.Utils;
using AIA.Training.Core.Configs;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Contracts.Repository;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.Models;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace AIA.Training.Business
{
    public partial class RoomBusiness : IRoomBusiness
    {
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public RoomBusiness(IRoomRepository repo, IMapper mapper, ILogger logger)
        : this(repo)
        {
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<GetRoomResult> GetRoomDetail(GetRoom query)
        {
            var entity = repository.List(q => q.Id == query.Id).FirstOrDefault();
            if (entity != null)
            {
                await Task.FromResult(_mapper.Map<GetRoomResult>(entity));
            }

            return null;
        }

        public async Task<ListResult<GetRoomResult>> GetAll(GetRooms query)
        {
            Expression<Func<Room, bool>> condition = x => true;

            if (query.Deleted != null)
            {
                condition = condition.And(x => x.Deleted == query.Deleted);
            }

            if (query.PageIndex == 0)
            {
                query.PageIndex = Constants.DEFAULT_PAGE_INDEX;
            }

            if (query.PageSize == 0)
            {
                query.PageSize = Constants.DEFAULT_PAGE_SIZE;
            }

            if (query.OrderBy == null || query.OrderBy.Count == 0)
            {
                query.OrderBy = new List<OrderBy>
                {
                    new OrderBy
                    {
                        PropertyName = nameof(Room.CreatedDateTime),
                        Desc = true
                    }
                };
            }
            var data = repository
                .List(condition, query.OrderBy, query.PageIndex, query.PageSize, out var total).ToList();

            var result = new ListResult<GetRoomResult>
            {
                Items = _mapper.Map<List<GetRoomResult>>(data),
                TotalRecords = total
            };

            return await Task.FromResult(result);        }

        public async Task CreateOrUpdate(CreateOrUpdateRoom command)
        {
            // Create
            if (string.IsNullOrEmpty(command.Id))
            {
                var room = new Room()
                {
                    RoomNumber = command.RoomNumber,
                    Location = command.Location
                };
                repository.Insert(room, true);
            }
            // Update
            else
            {
                var entity = repository.GetById(command.Id);
                if (entity == null)
                {
                    throw new NullReferenceException("Identity not in data of table room.");
                }

                entity.RoomNumber = command.RoomNumber;
                entity.Location = command.Location;
                repository.Update(entity, true);
            }

            await Task.CompletedTask;
        }

        public async Task DeleteRoom(DeleteRoom command)
        {
            repository.Delete(command.Id, true);
            await Task.CompletedTask;
        }
    }
}
