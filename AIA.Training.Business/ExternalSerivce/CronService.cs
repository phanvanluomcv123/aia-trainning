using AIA.Training.Core.Contracts.ExternalService;
using AIA.One.Core.Jobs;
using Hangfire;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AIA.Training.Business.ExternalSerivce
{
    public abstract class CronService<TService> : ICronService where TService : ICronService
    {
        protected IBackgroundJob _backgroundJob;
        protected ILogger _logger;
        protected readonly string _jobCode;
        protected readonly string _jobId;
        protected readonly string _cron;
        private readonly IJobWatcher _jobWatcher;

        protected CronService(IBackgroundJob backgroundJob, ILogger logger, string jobCode, string cron, IJobWatcher jobWatcher)
        {
            _backgroundJob = backgroundJob;
            _logger = logger;
            _jobCode = jobCode;
            _jobId = jobCode;
            _cron = cron;
            _jobWatcher = jobWatcher;
        }

        protected abstract void ExecuteInternal();

        protected virtual Task CleanUp()
        {
            return Task.CompletedTask;
        }

        public void Execute()
        {
            try
            {
                if (_jobWatcher.RunningJobs.Any(x => x == _jobCode))
                {
                    _logger.Information($"Skip runing job :{JsonConvert.SerializeObject(_jobWatcher.RunningJobs)}");
                    return;
                }

                _jobWatcher.RunningJobs.Add(_jobCode);
                this.ExecuteInternal();
                _jobWatcher.RunningJobs.Remove(_jobCode);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.Message);
            }
        }

        public Task RegisterAsync(CancellationToken token)
        {
            _logger.Information("{jobId}: Starting..", _jobId);
            Console.WriteLine("{0}: Starting..", _jobId);

            try
            {
                _backgroundJob.RemovJob(_jobCode);
            }
            catch (Exception ex)
            {
                this._logger.Error(ex, $"Could not remnove job {_jobCode}");
            }

            using (var connection = JobStorage.Current.GetConnection())
            {
                var monitorApi = JobStorage.Current.GetMonitoringApi();

                HashSet<string> queues = connection.GetAllItemsFromSet("recurring-jobs");
                if (!queues.Any(x => x == _jobCode))
                {
                    _backgroundJob.Recurring<TService>(_jobCode, job => job.Execute(), _cron);
                }
            }

            _logger.Information("{jobId}: Started", _jobId);
            Console.WriteLine("{0}: Started", _jobId);
            return Task.CompletedTask;
        }

        public async Task RemoveAsync(CancellationToken token)
        {
            _logger.Information("{jobId}: Removing job..", _jobId);
            Console.WriteLine("{0}: Removing job..", _jobId);
            await this.CleanUp();

            _backgroundJob.RemovJob(_jobCode);

            _logger.Information("{jobId}: Job removed!", _jobId);
            Console.WriteLine("{0}: Job removed!", _jobId);
        }
    }
}
