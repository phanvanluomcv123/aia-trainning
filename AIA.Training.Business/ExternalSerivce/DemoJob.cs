﻿using AIA.One.Core.Jobs;
using AIA.Training.Core.Configs;
using AIA.Training.Core.Contracts.ExternalService;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIA.Training.Business.ExternalSerivce
{
    public class DemoJob : CronService<IDemoJob>, IDemoJob
    {
        private const string _jobCode = Core.Configs.JobCode.Demo_Job_Code;
        private static readonly string _cron = "*/1 * * * *";

        private readonly ILogger _logger;
        public DemoJob(ILogger logger, IBackgroundJob backgroundJob, IJobWatcher jobWatcher) : base(backgroundJob, logger,_jobCode, _cron, jobWatcher)
        {
            _logger = logger;
        }
        protected override void ExecuteInternal()
        {
            Console.WriteLine("Hello");
        }
    }
}
