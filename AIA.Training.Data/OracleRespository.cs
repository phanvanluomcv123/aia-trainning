using AIA.One.Core;
using AIA.One.Core.Data.Dapper;
using Dapper;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace AIA.Training.Data
{
    public class OracleRespository<T> : DapperBaseRepository<T> where T : BaseEntity, new()
    {
        public OracleRespository(DbConnection connection) : base(connection)
        {
        }

        public override async Task<IEnumerable<T>> ListAsync(int page = 1, int size = 50)
        {
            return await _connection.QueryAsync<T>($"Select * from {_tableName} OFFSET {(page - 1) * size} ROWS FETCH NEXT {size} ROWS ONLY");
        }

        public override async Task<IEnumerable<T>> ListAsync(string query, object param = null)
        {
            return await _connection.QueryAsync<T>($"Select * from {_tableName} {query} ", new { param });
        }
    }
}
