﻿using AIA.Training.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace AIA.Training.Data
{
    public partial class CoreDataContext : DbContext
    {
        #region "Entities"
		public DbSet<Category> Category { get; set; }
		public DbSet<Room> Room { get; set; }
		public DbSet<RoomService> RoomService { get; set; }
		public DbSet<RoomStatus> RoomStatus { get; set; }
		public DbSet<RoomType> RoomType { get; set; }
		public DbSet<Student> Student { get; set; }
		#endregion

		public CoreDataContext(DbContextOptions<CoreDataContext> options) : base(options)
        {

        }
    }
}
