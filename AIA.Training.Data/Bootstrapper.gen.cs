﻿using AIA.Training.Core.Contracts.Repository;
using AIA.One.Core.DependencyInjection;
using System;

namespace AIA.Training.Data
{
    public class Bootstrapper : IWireUpDependencies
    {
	    protected readonly IRegisterDependencies registerDependencies;

        public Bootstrapper(IRegisterDependencies registerDependencies)
        {
            if (registerDependencies == null)
            {
                throw new ArgumentNullException("registerDependencies");
            }

            this.registerDependencies = registerDependencies;
        }

        public virtual void WireUp()
        {
            //this.registerDependencies.Register<IDbContext, CoreDataContext>();
            // register repository
            this.registerDependencies.Register<ICategoryRepository, CategoryRepository>();
            this.registerDependencies.Register<IRoomRepository, RoomRepository>();
            this.registerDependencies.Register<IRoomServiceRepository, RoomServiceRepository>();
            this.registerDependencies.Register<IRoomStatusRepository, RoomStatusRepository>();
            this.registerDependencies.Register<IRoomTypeRepository, RoomTypeRepository>();
            this.registerDependencies.Register<IStudentRepository, StudentRepository>();
        }
	}
}
