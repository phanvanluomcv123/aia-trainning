﻿03-05-2021
	- add relationship for course, category, class, session, document
	- add fields into course: isonline, daysOfReset, requiredChannel 
23-04-2021
	- add businesscode and replacement course fields [course]
	- update max lenght of type []
22-04-2021
	- update field office to position into base Entity
	- update and add fields into course
11-04-2021
	- Init db