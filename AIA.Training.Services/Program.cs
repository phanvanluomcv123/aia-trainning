using AIA.One.Core.Data;
using AIA.One.Core.DependencyInjection;
using AIA.One.Core.Jobs;
using AIA.One.Core.Messages;
using AIA.One.Core.UnitOfWork;
using AIA.Training.Core.Configs;
using AIA.Training.Core.Extensions;
using AIA.Training.Data;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Kledex.Bus;
using Kledex.Extensions;
using Kledex.Store.EF.InMemory;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Hangfire;
using AIA.One.Core.Configuration;
using Microsoft.Extensions.Caching.Memory;
using AIA.Training.Core.Http;
using AIA.Training.Infrastructure.Messages;
using AIA.Training.Infrastructure.Caching;
using AIA.Training.Services.Services;
using Kledex.Bus.ServiceBus.Extensions;
using Kledex.Store.EF.SqlServer.Extensions;
using AIA.Training.Core.Contracts.ExternalService;
using AIA.Training.Core.Models;

namespace AIA.Training.Services
{
    static class Program
    {
        public static string SERVICE_ID { get; private set; }
        public static IConfiguration Configuration { get; private set; }
        public static HangfireConfig _hangfireConfig = new HangfireConfig();

        static async Task Main(string[] args)
        {
            SERVICE_ID = Guid.NewGuid().ToString();

            Serilog.Debugging.SelfLog.Enable(Console.Error);

            try
            {
                var hostBuilder = new HostBuilder()
                               .UseServiceProviderFactory(new AutofacServiceProviderFactory(builder =>
                               {
                                   Console.WriteLine("configure action for AutofacServiceProviderFactory");
                               }))
                               .ConfigureAppConfiguration((hostContext, configApp) =>
                               {
                                   ConfigApp(args, configApp);
                               })
                               .ConfigureServices((hostContext, services) =>
                               {
                                   ConfigServices(services);
                               })
                               .ConfigureContainer((Action<ContainerBuilder>)(builder =>
                               {
                                   ConfigContainer(builder);
                               }));

                var host = hostBuilder.Build();

                Log.Information($"********** [AIA.Training.Services] service host [{SERVICE_ID}] is starting **********");

                await host.StartAsync();

                Log.Information("********** [AIA.Training.Services] hosted service started **********");

                await host.WaitForShutdownAsync();
                host.Dispose();
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "[Program] start fail");
                Console.WriteLine(ex);
            }
            await Task.CompletedTask;
        }

        private static void ConfigApp(string[] args, IConfigurationBuilder configApp)
        {
            configApp.AddCommandLine(args);
            configApp.AddEnvironmentVariables();
            configApp.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);

            Configuration = new ConfigurationBuilder().AddJsonFile($"appsettings.json").Build();
        }

        private static void ConfigServices(IServiceCollection services)
        {
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            AppSettings.Instance = appSettingsSection.Get<AppSettings>();
            Log.Logger.Information(JsonConvert.SerializeObject(AppSettings.Instance));

            services.AddHostedService<HostedServices>();

            //services.AddDbContext<DbContext, CoreDataContext>(o => o.UseOracle(AppSettings.Instance.CoreDb));

            // Cqrs
            services.AddKledex(typeof(CommandHandler.Bootstrapper))
                   .AddServiceBus()
                   .AddSqlServerStore();

            _hangfireConfig.Init(AppSettings.Instance.BackgroundJobType,
                AppSettings.Instance.BackgroundJobConnection,
                services);
        }

        private static void ConfigContainer(ContainerBuilder autofacBuilder)
        {
            var builder = new CoreContainerAdapter(autofacBuilder);

            builder.RegisterInstance<IRegisterDependencies>(builder);
            builder.RegisterInstance<IResolveDependencies>(builder);
            builder.RegisterInstance<ILogger>(Log.Logger);
            builder.RegisterInstance<IJobWatcher>(new JobWatcher());
            builder.RegisterPerLifetime<IUnitOfWork, UnitOfWork>();
            builder.RegisterPerLifetime<IBackgroundJob, HangfireJob>();
            builder.RegisterSingleton<IMemoryCache, MemoryCache>();

            //builder.RegisterPerLifetime<IOracleCoreDataDapperContext, OracleCoreDataDapperContext>(
            //  new DependencyParameter("connectionString", AppSettings.Instance.CoreDb),
            //  new DependencyParameter("logger", Log.Logger));

            var mapper = MapperConfig.Init();
            builder.RegisterInstance<IMapper>(mapper);

            //new Data.ManualBootstrapper(builder).WireUp();
            new Business.ManualBootstrapper(builder).WireUp();
            //new Validators.ManualBootstrapper(builder).WireUp();
            //new Infrastructure.ManualBootstrapper(builder, Log.Logger).WireUp();

            autofacBuilder.RegisterBuildCallback(autofacContainer =>
            {
                builder.SetContainerInstance(autofacContainer as IContainer);

                var bgJobServerOptions = new BackgroundJobServerOptions
                {
                    WorkerCount = Math.Min(Environment.ProcessorCount * 5, 20)
                };
                _hangfireConfig.StartServerWithAutofacContainer(builder.ContainerInstance, bgJobServerOptions);
            });

            DependencyResolver.SetResolver(builder);
        }
    }
}
