using AIA.Training.Core.Configs;
using Microsoft.ApplicationInsights.WorkerService;
using Microsoft.Extensions.DependencyInjection;

namespace AIA.Training.Services
{
    public static class WorkerServiceApplicationInsightsConfigureServicesExtension
    {
        /// <summary>
        /// Configure the AI service with specific connection string and service options
        /// https://docs.microsoft.com/en-us/azure/azure-monitor/app/worker-service#aspnet-core-background-tasks-with-hosted-services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="appSettings"></param>
        public static void AddAndConfigureApplicationInsightsServiceForWorkerService(this IServiceCollection services, AppSettings appSettings)
        {
            if (appSettings == null || string.IsNullOrEmpty(appSettings.ApplicationInsightsConnectionString))
            {
                // Try to load the configuration from ApplicationInsights section
                services.AddApplicationInsightsTelemetryWorkerService();
            }
            else
            {
                var aiOptions = new ApplicationInsightsServiceOptions
                {
                    ConnectionString = appSettings.ApplicationInsightsConnectionString
                };
                services.AddApplicationInsightsTelemetryWorkerService(aiOptions);
            }
        }
    }
}
