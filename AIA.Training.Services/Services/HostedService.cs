﻿using AIA.Training.Core.Contracts.ExternalService;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace AIA.Training.Services.Services
{
    public class HostedServices : IHostedService
    {
        private readonly IDemoJob _demoJob;
        //....inject job 
        public HostedServices(IDemoJob demoJob)
        {
            _demoJob = demoJob;
        }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _demoJob.RegisterAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _demoJob.RemoveAsync(cancellationToken);
        }
    }
}
