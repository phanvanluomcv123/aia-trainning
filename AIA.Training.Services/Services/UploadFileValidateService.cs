using AIA.One.Core.Jobs;
using AIA.Training.Core.Configs;
using AIA.One.SDKs;
using AutoMapper;
using Kledex;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AIA.Training.Services
{
    public class UploadFileValidateService : IHostedService
    {
        private IBackgroundJob _job;
        private IDispatcher _dispatcher;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private const string JOB_VALIDATE_FILE_ID = "Job_UploadFileValidate";
        private string _jobUniqueId;
        private string _jobDir;
        private Dictionary<Type, string> _formatStyles;

        public UploadFileValidateService(IBackgroundJob job, IDispatcher dispatcher, IMapper mapper,
            ILogger logger)
        {
            _job = job;
            _dispatcher = dispatcher;
            _mapper = mapper;
            _logger = logger;
            _jobUniqueId = $"{JOB_VALIDATE_FILE_ID}_{Program.SERVICE_ID}";
            _formatStyles = new Dictionary<Type, string>
            {
                { typeof(DateTime), Constants.DATE_IMPORT_FORMAT }
            };
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _job.Recurring(_jobUniqueId, () => ReadFiles(), Hangfire.Cron.Minutely());

            await Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _job.RemovJob(_jobUniqueId);

            await Task.CompletedTask;
        }

        public void ReadFiles()
        {
        }
    }
}
