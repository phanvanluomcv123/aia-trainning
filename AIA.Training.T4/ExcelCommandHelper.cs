using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIA.Training.T4
{
    public class ExcelCommandHelper
    {
        private readonly Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim();
        private readonly Func<object, string> convertStringToLower = value => value == null ? "" : value.ToString().Trim().ToLower();
        private int startRowIdx = 1, startRowPropertyIdx = 10, startColIdx = 1;

        public List<CommmandDescription> BuildCommandDescriptions(params string[] excelPaths)
        {
            var result = new List<CommmandDescription>();
            foreach (var xlWorkbook in excelPaths.Select(excelPath => new Workbook(excelPath)))
            {
                for (var i = 1; i < xlWorkbook.Worksheets.Count; i++)
                {
                    try
                    {
                        result.Add(GenerateCommand(xlWorkbook.Worksheets[i]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                xlWorkbook.Dispose();
            }

            return result;
        }

        public List<string> BuildMigrateRepository()
        {
            return new List<string>()
            {
                "SecondaryCustomer","CodeMaster","Customer"
            };
        }

        public CommmandDescription GenerateCommand(Worksheet sheet)
        {
            Cells xlRange = sheet.Cells;

            Func<string, string> getCommandBusType = (text) =>
            {
                switch (text)
                {
                    case "1":
                        return "Async";
                    case "2":
                        return "Rpc";
                    default:
                        return "Sync";
                }
            };

            int tmpStartRowIdx = startRowIdx;
            string commandStr = convertString(xlRange[tmpStartRowIdx, 1].Value);
            string eventStr = convertString(xlRange[++tmpStartRowIdx, 1].Value);
            string eventHandlerStr = convertString(xlRange[++tmpStartRowIdx, 1].Value);
            string aggregateRootStr = convertString(xlRange[++tmpStartRowIdx, 1].Value);
            string entityStr = convertString(xlRange[++tmpStartRowIdx, 1].Value);
            string serviceStr = convertString(xlRange[++tmpStartRowIdx, 1].Value);
            string isCreatingCommandStr = convertStringToLower(xlRange[++tmpStartRowIdx, 1].Value);
            string commandBusType = convertString(xlRange[++tmpStartRowIdx, 1].Value);

            tmpStartRowIdx = startRowIdx;
            string controllerStr = convertString(xlRange[tmpStartRowIdx, 3].Value);
            string routeStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);
            string actionStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);
            string descriptionStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);
            string roleStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);
            string permissionStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);
            string attrStr = convertString(xlRange[++tmpStartRowIdx, 3].Value);

            var modelDescription = new CommmandDescription()
            {
                Command = commandStr,
                Event = eventStr,
                EventHandler = eventHandlerStr,
                AggregateRoot = aggregateRootStr,
                Entity = entityStr,
                Service = serviceStr,
                IsCreatingCommand = (isCreatingCommandStr == "x"),
                CommandBusType = getCommandBusType(commandBusType),
                Controller = controllerStr,
                Route = routeStr,
                Action = actionStr,
                Roles = roleStr.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(x => string.Join(",", x.Split(',').Select(r => $"\"{r.Trim()}\""))).ToArray(),
                Permissions = permissionStr.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(x => string.Join(",", x.Split(',').Select(p => $"\"{p.Trim()}\""))).ToArray(),
                Attributes = attrStr.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
            };

            modelDescription.Method = "HttpPost";
            if (modelDescription.Route.Contains("{id}"))
            {
                modelDescription.Method = "HttpPut";
            }

            for (int rowIndex = startRowPropertyIdx; rowIndex <= xlRange.Rows.Count; rowIndex++)
            {
                var property = GenerateProperty(xlRange, rowIndex, startColIdx);
                if (property != null)
                {
                    modelDescription.Properties.Add(property);
                }
                else
                {
                    break;
                }
            }

            return modelDescription;
        }

        public CommmandDescriptionProperty GenerateProperty(Cells cells, int rowIndex, int colIdx)
        {
            var propertyName = convertString(cells[rowIndex, colIdx].Value);
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return null;
            }

            var result = new CommmandDescriptionProperty();
            result.Type = convertString(cells[rowIndex, ++colIdx].Value);
            result.IsCustomType = convertStringToLower(cells[rowIndex, ++colIdx].Value) == "x";
            result.Required = convertStringToLower(cells[rowIndex, ++colIdx].Value) == "x";
            result.Regex = convertStringToLower(cells[rowIndex, ++colIdx].Value);
            string rangeStr = convertStringToLower(cells[rowIndex, ++colIdx].Value);
            if (!string.IsNullOrWhiteSpace(rangeStr))
            {
                string minStr = "", maxStr = "";
                var arr = rangeStr.Split('-');
                if (arr.Length > 1)
                {
                    result.Range = new RangeObject();

                    minStr = arr[0].Trim();
                    maxStr = arr[1].Trim();
                }

                if (!string.IsNullOrWhiteSpace(minStr))
                {
                    result.Range.Min = int.Parse(minStr);
                }

                if (!string.IsNullOrWhiteSpace(maxStr))
                {
                    result.Range.Max = int.Parse(maxStr);
                }
            }

            result.PropertyName = propertyName;

            return result;
        }

    }

    public class CommmandDescription
    {
        public CommmandDescription()
        {
            Properties = new List<CommmandDescriptionProperty>();
        }

        public string Command { get; set; }

        public string Service { get; set; }

        public string Event { get; set; }

        public string EventHandler { get; set; }

        public string AggregateRoot { get; set; }

        public string Entity { get; set; }

        public bool IsCreatingCommand { get; set; }

        public string CommandBusType { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string Route { get; set; }

        public string Method { get; set; }

        public string Description { get; set; }

        public string[] Roles { get; set; }
        public string[] Permissions { get; set; }
        public string[] Attributes { get; set; }
        public List<CommmandDescriptionProperty> Properties { get; set; }
    }

    public class CommmandDescriptionProperty
    {
        public string PropertyName { get; set; }
        public string Type { get; set; }
        public bool IsCustomType { get; set; }
        public bool Required { get; set; }
        public string Regex { get; set; }
        public RangeObject Range { get; set; }
    }

    public class RangeObject
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
