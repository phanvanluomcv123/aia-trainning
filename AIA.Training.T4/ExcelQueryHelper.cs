using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AIA.Training.T4
{
    public class ExcelQueryHelper
    {
        private readonly Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim();
        private readonly Func<object, string> convertStringToLower = value => value == null ? "" : value.ToString().Trim().ToLower();
        private int startRowIdx = 12, startColQueryIdx = 1, startColResponseIdx = 6;

        public List<QueryDescription> BuildQueryDescriptions(params string[] excelPaths)
        {
            var result = new List<QueryDescription>();
            foreach (var xlWorkbook in excelPaths.Select(excelPath => new Workbook(excelPath)))
            {
                for (var i = 1; i < xlWorkbook.Worksheets.Count; i++)
                {
                    try
                    {
                        result.Add(GenerateQuery(xlWorkbook.Worksheets[i]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                xlWorkbook.Dispose();
            }

            return result;
        }

        public QueryDescription GenerateQuery(Worksheet sheet)
        {
            Cells xlRange = sheet.Cells;
            var roleStr = convertString(xlRange[7, 1].Value);
            var permissionStr = convertString(xlRange[8, 1].Value);
            var attrStr = convertString(xlRange[9, 1].Value);

            var modelDescription = new QueryDescription()
            {
                Controller = convertString(xlRange[1, 1].Value),
                Query = convertString(xlRange[2, 1].Value),
                Response = convertString(xlRange[3, 1].Value),
                Route = convertString(xlRange[4, 1].Value),
                Action = convertString(xlRange[5, 1].Value),
                Description = convertString(xlRange[6, 1].Value),
                Roles = roleStr.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(x => string.Join(",", x.Split(',').Select(r => $"\"{r.Trim()}\""))).ToArray(),
                Permissions = permissionStr.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Select(x => string.Join(",", x.Split(',').Select(p => $"\"{p.Trim()}\""))).ToArray(),
                Attributes = attrStr.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
            };

            modelDescription.ResponseType = convertString(xlRange[3, 1].Value);
            var match = Regex.Match(modelDescription.ResponseType, @"<\w*>");
            if (match.Success)
            {
                modelDescription.ResponseType = match.Value.Replace("<", "").Replace(">", "");
            }

            modelDescription.Method = "HttpPost";
            if (modelDescription.Route.Contains("{id}"))
            {
                modelDescription.Method = "HttpPut";
            }

            for (int rowIndex = startRowIdx; rowIndex <= xlRange.Rows.Count; rowIndex++)
            {
                var queryProperty = GenerateProperty(xlRange, rowIndex, startColQueryIdx);
                var responseProperty = GenerateProperty(xlRange, rowIndex, startColResponseIdx);
                var isExistQueryProperty = queryProperty != null;
                var isExistResponseProperty = responseProperty != null;

                if (isExistQueryProperty)
                {
                    modelDescription.QueryProperties.Add(queryProperty);
                }
                
                if(isExistResponseProperty)
                {
                    modelDescription.ResponseProperties.Add(responseProperty);
                }

                if(!isExistQueryProperty && !isExistResponseProperty)
                {
                    break;
                }
            }

            return modelDescription;
        }

        public QueryDescriptionProperty GenerateProperty(Cells cells, int rowIndex, int colIdx)
        {
            var propertyName = convertString(cells[rowIndex, colIdx].Value);
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return null;
            }

            var result = new QueryDescriptionProperty();
            result.Type = convertString(cells[rowIndex, ++colIdx].Value);
            result.Required = convertStringToLower(cells[rowIndex, ++colIdx].Value) == "x";
            result.PropertyName = propertyName;

            return result;
        }

    }

    public class QueryDescription
    {
        public QueryDescription()
        {
            QueryProperties = new List<QueryDescriptionProperty>();
            ResponseProperties = new List<QueryDescriptionProperty>();
        }

        public string Controller { get; set; }
        public string Query { get; set; }
        public string Response { get; set; }
        public string ResponseType { get; set; }
        public string Route { get; set; }
        public string Method { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string[] Roles { get; set; }
        public string[] Permissions { get; set; }
        public string[] Attributes { get; set; }

        public List<QueryDescriptionProperty> QueryProperties { get; set; }
        public List<QueryDescriptionProperty> ResponseProperties { get; set; }
    }

    public class QueryDescriptionProperty
    {
        public string PropertyName { get; set; }
        public string Type { get; set; }
        public bool Required { get; set; }
    }
}
