using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AIA.Training.T4
{
    public class ExcelEventHelper
    {
        private readonly Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim();
        private readonly Func<object, string> convertStringToLower = value => value == null ? "" : value.ToString().Trim().ToLower();

        public List<EventDescription> BuildEventDescriptions(params string[] excelPaths)
        {
            var result = new List<EventDescription>();
            foreach (var xlWorkbook in excelPaths.Select(excelPath => new Workbook(excelPath)))
            {
                for (var i = 1; i < xlWorkbook.Worksheets.Count; i++)
                {
                    try
                    {
                        result.Add(GenerateCommand(xlWorkbook.Worksheets[i]));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                xlWorkbook.Dispose();
            }

            return result;
        }

        public EventDescription GenerateCommand(Worksheet sheet)
        {
            Cells xlRange = sheet.Cells;
            string eventStr = convertString(xlRange[1, 1].Value);

            var modelDescription = new EventDescription()
            {
                Event = eventStr
            };

            for (int rowIndex = 10; rowIndex <= xlRange.Rows.Count; rowIndex++)
            {
                var property = GenerateProperty(xlRange, rowIndex);
                if (property != null)
                {
                    modelDescription.Properties.Add(property);
                }
                else
                {
                    break;
                }
            }

            return modelDescription;
        }

        public EventDescriptionProperty GenerateProperty(Cells cells, int rowIndex)
        {
            var propertyName = convertString(cells[rowIndex, 1].Value);
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return null;
            }

            var type = cells[rowIndex, 2].Value;
            var result = new EventDescriptionProperty();
            result.IsCustomType = convertStringToLower(cells[rowIndex, 3].Value) == "x";
            result.PropertyName = propertyName;
            result.Type = convertString(type);
            return result;
        }

    }

    public class EventDescription
    {
        public EventDescription()
        {
            Properties = new List<EventDescriptionProperty>();
        }

        public string Event { get; set; }

        public List<EventDescriptionProperty> Properties { get; set; }
    }

    public class EventDescriptionProperty
    {
        public string PropertyName { get; set; }
        public string Type { get; set; }
        public bool IsCustomType { get; set; }
    }
}
