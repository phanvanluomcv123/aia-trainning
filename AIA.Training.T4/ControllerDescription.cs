using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIA.Training.T4
{
    public class ControllerDescription
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Route { get; set; }
        public string Method { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string[] Roles { get; set; }
        public string[] Permissions { get; set; }
        public string[] Attributes { get; set; }
        public string Description { get; set; }
        public bool IsQuery { get; set; }
    }
}
