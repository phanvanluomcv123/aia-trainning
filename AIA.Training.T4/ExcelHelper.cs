using Aspose.Cells;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;


namespace AIA.Training.T4
{
    public class ExcelHelper
    {
        public readonly static List<string> BaseTypes = new List<string>
        {
            "string",
            "int",
            "int16",
            "int32",
            "int64",
            "long",
            "ulong",
            "uint",
            "float",
            "double",
            "decimal",
            "datetime",
            "bool"
        };

        Dictionary<string, Type> typeMappingDictionary = new Dictionary<string, Type>()
            {
                { "bigint",  typeof(Int64) },
                { "binary",  typeof(byte[]) },
                { "bit",  typeof(Boolean) },
                { "char",  typeof(String) },
                { "date",  typeof(DateTime) },
                { "datetime",  typeof(DateTime) },
                { "datetime2",  typeof(DateTime) },
                { "datetimeoffset",  typeof(DateTimeOffset) },
                { "decimal",  typeof(Decimal) },
                { "varbinary",  typeof(Byte[]) },
                { "float",  typeof(Double) },
                { "image",  typeof(Byte[]) },
                { "int",  typeof(Int32) },
                { "money",  typeof(Decimal) },
                { "nchar",  typeof(String) },
                { "ntext",  typeof(String) },
                { "numeric",  typeof(Decimal) },
                { "nvarchar",  typeof(String) },
                { "real",  typeof(Single) },
                { "rowversion",  typeof(Byte[]) },
                { "smalldatetime",  typeof(DateTime) },
                { "smallint",  typeof(Int16) },
                { "smallmoney",  typeof(Decimal) },
                { "text",  typeof(String) },
                { "time",  typeof(TimeSpan) },
                { "timestamp",  typeof(Byte[]) },
                { "tinyint",  typeof(Byte) },
                { "uniqueidentifier",  typeof(Guid) },
                { "varchar",  typeof(String) },
                { "boolean",  typeof(Boolean) }
            };

        public List<ModelDescription> BuildModelDescriptions(string excelPath)
        {
            var result = new List<ModelDescription>();

            var fileInfo = File.OpenRead(excelPath);
            Workbook xlWorkbook = new Workbook(fileInfo);

            for (int i = 1; i < xlWorkbook.Worksheets.Count; i++)
            {
                try
                {
                    result.Add(GenerateModel(xlWorkbook.Worksheets[i]));
                }
                catch (Exception e)
                {
                    //LogUtils.WriteLog(e.ToString());
                    Console.WriteLine(e);
                }
            }

            // Update children name
            /*
            foreach (var m in result)
            {
                if (!string.IsNullOrWhiteSpace(m.ParentName))
                {
                    var parent = result.FirstOrDefault(item => item.ModelName == m.ParentName);
                    if (parent != null)
                        parent.ChildrenName.Add(m.ModelName);
                }
            }
            */
            result = result.OrderBy(m => m.ModelName).ToList();
            xlWorkbook.Dispose();
            return result;
        }



        public ModelDescription GenerateModel(Worksheet sheet)
        {
            Cells xlRange = sheet.Cells;


            Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim().ToLower();
            Func<object, string> convertStringNormal = value => value == null ? "" : value.ToString().Trim();
            string modelName = xlRange[4, 1].Value.ToString();

            var modelIndexingTypeName = string.Empty;
            var modelIndexingTypeNameCell = xlRange[3, 1];
            if (!string.IsNullOrWhiteSpace(modelIndexingTypeNameCell.Value as string) && convertString(modelIndexingTypeNameCell.Value) != "x")
            {
                modelIndexingTypeName = modelIndexingTypeNameCell.Value.ToString();
            }


            string searchParentModelName = convertStringNormal(xlRange[4, 4].Value);

            string viewName = string.Empty;
            if (!String.IsNullOrWhiteSpace(xlRange[4, 11].Value as string))
                viewName = xlRange[4, 11].Value.ToString();

            var interfaces = "";
            if (!string.IsNullOrWhiteSpace(xlRange[4, 3].Value as string))
            {
                interfaces = xlRange[4, 3].Value.ToString();
            }

            var modelDescription = new ModelDescription(modelName, modelIndexingTypeName)
            {
                ViewName = viewName,
                Interfaces = interfaces.Split(',', ';').Select(i => i.Trim()).ToList(),
                LazyChild = convertString(xlRange[4, 4].Value) == "x",
                SearchParentModelName = searchParentModelName
            };

            if(xlRange[2, 1].Value as string == "x")
            {
                modelDescription.IsModelNameStandardized = true;
            }

            if (modelDescription.LazyChild)
            {
                modelDescription.Interfaces.Add("AIA.One.FusionCenter.Sdk.Business.ILazyChild");

                modelDescription.PropertyDescriptions.Add(new PropertyDesciption
                {
                    AliasName = "",
                    PropertyName = "ParentId",
                    DotNetType = typeof(String),
                    Length = "255",
                    Required = true,
                    Server = true,
                    Phone = true,
                    ServerIndex = true,
                    IsPrimary = true
                });
            }

            for (int rowIndex = 6; rowIndex <= (xlRange.LastCell?.Row ?? 0); rowIndex++)
            {
                var property = GenerateProperty(xlRange, rowIndex);
                if (property != null)
                {
                    modelDescription.ServerModel = (modelDescription.ServerModel || property.Server);
                    modelDescription.PhoneModel = (modelDescription.PhoneModel || property.Phone);
                    modelDescription.TransactionModel = (modelDescription.TransactionModel || property.PropertyName.ToLower() == "syncstatus");
                    modelDescription.PropertyDescriptions.Add(property);

                    if (property.Required && property.CustomType != null)
                    {
                        if (property.CustomType.StartsWith("ICollection"))
                            modelDescription.ChildrenName.Add(property.CustomType.Replace("ICollection<", "").Replace(">", ""));
                        else
                            modelDescription.ParentName.Add(property.CustomType);
                    }
                }
                else
                {
                    break;
                }
            }

            var inheritFrom = modelDescription.PropertyDescriptions.Any(x => x.Standardized) ? "BaseEntityS" : "BaseEntity";
            if (!String.IsNullOrWhiteSpace(xlRange[4, 2].Value as string))
            {
                inheritFrom = xlRange[4, 2].Value.ToString();
            }

            modelDescription.InheritFrom = inheritFrom;
            modelDescription.LocationProperties = modelDescription.PropertyDescriptions.Where(p => !string.IsNullOrWhiteSpace(p.CustomType) && p.CustomType.EndsWith("GeoPoint")).ToList();

            return modelDescription;
        }

        public PropertyDesciption GenerateProperty(Cells cells, int rowIndex)
        {
            if (cells[rowIndex, 1].Value == null)
            {
                return null;
            }

            var result = new PropertyDesciption();

            Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim().ToLower();
            result.AliasName = cells[rowIndex, 0].Value == null ? string.Empty : cells[rowIndex, 0].Value.ToString().Trim();
            result.PropertyName = cells[rowIndex, 1].Value.ToString().Trim().Replace(" ", "");
            string sqlType = convertString(cells[rowIndex, 2].Value);
            if (typeMappingDictionary.ContainsKey(sqlType))
            {
                result.DotNetType = typeMappingDictionary[sqlType];
            }
            else
            {
                result.CustomType = cells[rowIndex, 2].Value as string;
            }
            result.IsPrimary = convertString(cells[rowIndex, 3].Value) == "x";
            result.Length = convertString(cells[rowIndex, 4].Value);
            result.Required = convertString(cells[rowIndex, 5].Value) == "x";
            result.Validation = convertString(cells[rowIndex, 6].Value);
            result.Server = convertString(cells[rowIndex, 7].Value) == "x";
            result.Phone = convertString(cells[rowIndex, 8].Value) == "x";
            //result.ServerIndex = convertString(cells[rowIndex, 9].Value) == "x";
            // always set serverIndexing to false
            result.ServerIndex = false;
            result.PhoneIndex = convertString(cells[rowIndex, 10].Value) == "x";
            result.View = convertString(cells[rowIndex, 11].Value) == "x";
            result.Unsign = convertString(cells[rowIndex, 12].Value) == "x";
            result.Standardized = convertString(cells[rowIndex, 15].Value) == "x";
            return result;
        }

        public List<ODataModelIntercepter> GetODataModelIntercepters(string excelPath)
        {
            var result = new List<ODataModelIntercepter>();
            Workbook xlWorkbook = new Workbook(excelPath);
            for (int i = 2; i <= xlWorkbook.Worksheets.Count; i++)
            {
                result.Add(GenerateModelIntercepter(xlWorkbook.Worksheets[i]));
            }
            result = result.OrderBy(m => m.ModelName).ToList();
            xlWorkbook.Dispose();
            return result;
        }

        private ODataModelIntercepter GenerateModelIntercepter(Worksheet sheet)
        {
            Cells xlRange = sheet.Cells;
            var modelDescription = new ODataModelIntercepter
            {
                ModelName = xlRange[1, 1].Value.ToString(),
                QueryAlias = "item"
            };

            if (!String.IsNullOrWhiteSpace(xlRange[1, 2].Value as string))
                modelDescription.QueryAlias = xlRange[1, 2].Value.ToString();

            for (int rowIndex = 4; rowIndex <= xlRange.Rows.Count; rowIndex++)
            {
                var property = GeneratePermissionIntercepter(xlRange, rowIndex);
                if (property != null)
                {
                    modelDescription.Permissions.Add(property);
                    if (!string.IsNullOrWhiteSpace(property.QueryExpression))
                    {
                        modelDescription.GenerateQueryInterceptor = true;
                    }
                    if (!property.CanInsert)
                    {
                        modelDescription.GenerateInsertInterceptor = true;
                    }
                    if (!property.CanDelete)
                    {
                        modelDescription.GenerateDeleteInterceptor = true;
                    }
                    if (!property.CanUpdate)
                    {
                        modelDescription.GenerateUpdateInterceptor = true;
                    }
                }
                else
                {
                    break;
                }
            }
            modelDescription.GenerateChangeInterceptor = modelDescription.GenerateInsertInterceptor || modelDescription.GenerateDeleteInterceptor
                || modelDescription.GenerateUpdateInterceptor;
            return modelDescription;
        }

        private ODataPermissionIntercepter GeneratePermissionIntercepter(Cells cells, int rowIndex)
        {
            if (string.IsNullOrWhiteSpace(cells[rowIndex, 1].Value as string))
            {
                return null;
            }

            var result = new ODataPermissionIntercepter();
            Func<object, string> convertString = value => value == null ? "" : value.ToString().Trim();

            result.PermissionName = cells[rowIndex, 1].Value.ToString().Trim();
            result.QueryExpression = convertString(cells[rowIndex, 2].Value);
            result.CanInsert = convertString(cells[rowIndex, 3].Value).ToLower() == "x";
            result.CanDelete = convertString(cells[rowIndex, 4].Value).ToLower() == "x";
            result.CanUpdate = convertString(cells[rowIndex, 5].Value).ToLower() == "x";
            return result;
        }
    }

    public class ModelDescription
    {
        public string ModelName { get; set; }
        public string ModelIndexingTypeName { get; set; }
        public string InheritFrom { get; set; }
        public string SearchParentModelName { get; set; }
        public string ViewName { get; set; }
        public bool ServerModel { get; set; }
        public bool PhoneModel { get; set; }
        public bool TransactionModel { get; set; }
        public bool LazyChild { get; set; }
        public bool IsModelNameStandardized { get; set; }
        public List<string> Interfaces { get; set; }
        public List<string> ParentName { get; set; }
        public List<string> ChildrenName { get; set; }
        public List<string> OptionalChildren { get; set; }
        public List<PropertyDesciption> PropertyDescriptions { get; set; }
        public List<PropertyDesciption> LocationProperties { get; set; }

        public bool HasLocation
        {
            get
            {
                return PropertyDescriptions.Any(p => p.PropertyName == "Longitude") &&
                       PropertyDescriptions.Any(p => p.PropertyName == "Latitude");
            }
        }

        public bool LocationServerIndex
        {
            get
            {
                return PropertyDescriptions.Any(p => p.PropertyName == "Longitude" && p.ServerIndex) &&
                       PropertyDescriptions.Any(p => p.PropertyName == "Latitude" && p.ServerIndex);
            }
        }

        public bool ServerIndex
        {
            get { return false; }// PropertyDescriptions.Any(p => p.ServerIndex); }
        }

        public ModelDescription(string modelName, string modelIndexingTypeName)
            : this()
        {
            ModelName = modelName;
            ModelIndexingTypeName = string.IsNullOrWhiteSpace(modelIndexingTypeName) ? modelName : modelIndexingTypeName;
            InheritFrom = "BaseEntity";
            ChildrenName = new List<string>();
            ParentName = new List<string>();
            OptionalChildren = new List<string>();
            TransactionModel = false;
        }

        public ModelDescription()
        {
            PropertyDescriptions = new List<PropertyDesciption>();
            LocationProperties = new List<PropertyDesciption>();
            PhoneModel = false;
            ServerModel = false;
            TransactionModel = false;
        }
    }

    public class PropertyDesciption
    {
        public string PropertyName { get; set; }
        public string AliasName { get; set; }
        public Type DotNetType { get; set; }
        public string CustomType { get; set; }
        public bool IsPrimary { get; set; }
        public string Length { get; set; }
        public bool Required { get; set; }
        public string Validation { get; set; }
        public bool Server { get; set; }
        public bool Phone { get; set; }
        public bool View { get; set; }
        public bool ServerIndex { get; set; }
        public bool PhoneIndex { get; set; }
        public string PropertyType { get; set; }
        public bool Unsign { get; set; }
        public bool Standardized { get; set; }

        public bool IsDatetime
        {
            get { return DotNetType != null && DotNetType == typeof(DateTime); }
        }

        public string FieldName
        {
            get { return char.ToLower(PropertyName[0]) + PropertyName.Substring(1); }
        }
    }

    public class ODataModelIntercepter
    {
        public string ModelName { get; set; }
        public string QueryAlias { get; set; }
        public bool GenerateQueryInterceptor { get; set; }
        public bool GenerateChangeInterceptor { get; set; }
        public bool GenerateUpdateInterceptor { get; set; }
        public bool GenerateInsertInterceptor { get; set; }
        public bool GenerateDeleteInterceptor { get; set; }
        public List<ODataPermissionIntercepter> Permissions { get; set; }

        public ODataModelIntercepter()
        {
            Permissions = new List<ODataPermissionIntercepter>();
        }
    }

    public class ODataPermissionIntercepter
    {
        public string PermissionName { get; set; }
        public string QueryExpression { get; set; }
        public bool CanInsert { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
    }

}
