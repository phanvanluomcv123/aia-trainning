﻿using AIA.One.Core.DependencyInjection;
using System;
using System.Collections.Generic;

namespace AIA.Training.EventHandler
{
    public class Bootstrapper : IWireUpDependencies
    {
        protected readonly IRegisterDependencies registerDependencies;

        public Bootstrapper(IRegisterDependencies registerDependencies)
        {
            this.registerDependencies = registerDependencies;
        }

        public virtual void WireUp()
        {
            // register service
            //registerDependencies.Register(typeof());
			//eventBus.Subscribe<CreatedOrUpdatedCategory, >();
			//eventBus.Subscribe<CreateOrUpdateRoom, >();
			//eventBus.Subscribe<DeleteRoom, >();
        }
	}
}
