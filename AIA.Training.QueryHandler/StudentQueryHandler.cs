using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AIA.One.Core.CQRS.Models;
using Kledex.Queries;
using System.Threading.Tasks;

namespace AIA.Training.QueryHandler
{
    public class StudentQueryHandler : IQueryHandlerAsync<GetStudent, ListResult<GetStudentResult>>
    {
        private readonly IStudentBusiness _business;

        public StudentQueryHandler(IStudentBusiness business)
        {
            _business = business;
        }

        public async Task<ListResult<GetStudentResult>> HandleAsync(GetStudent query)
        {
            return await _business.GetAll(query);
        }
    }
}
