﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AIA.One.Core.CQRS.Models;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using Kledex.Queries;

namespace AIA.Training.QueryHandler
{
    public class RoomQueryHandler 
        : IQueryHandlerAsync<GetRoom, GetRoomResult>, IQueryHandlerAsync<GetRooms, ListResult<GetRoomResult>>
    {
        private readonly IRoomBusiness _roomBusiness;

        public RoomQueryHandler(IRoomBusiness roomBusiness)
        {
            _roomBusiness = roomBusiness;
        }

        public async Task<GetRoomResult> HandleAsync(GetRoom query)
        {
            return await _roomBusiness.GetRoomDetail(query);
        }

        public async Task<ListResult<GetRoomResult>> HandleAsync(GetRooms query)
        {
            return await _roomBusiness.GetAll(query);
        }
    }
}
