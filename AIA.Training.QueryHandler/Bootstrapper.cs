using AIA.One.Core.DependencyInjection;

namespace AIA.Training.QueryHandler
{
    public class Bootstrapper : IWireUpDependencies
    {
        private readonly IRegisterDependencies container;

        public Bootstrapper(IRegisterDependencies container)
        {
            this.container = container;
        }

        public void WireUp()
        {
        }
    }
}
