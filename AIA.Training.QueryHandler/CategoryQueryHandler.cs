using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AIA.One.Core.CQRS.Models;
using Kledex.Queries;
using System.Threading.Tasks;

namespace AIA.Training.QueryHandler
{
    public class CategoryQueryHandler : IQueryHandlerAsync<GetCategory, GetCategoryResult>,
        IQueryHandlerAsync<GetCategories, ListResult<GetCategoryResult>>
    {
        private readonly ICategoryBusiness _business;

        public CategoryQueryHandler(ICategoryBusiness business)
        {
            _business = business;
        }

        public async Task<GetCategoryResult> HandleAsync(GetCategory query)
        {
            return await _business.GetDetail(query);
        }

        public async Task<ListResult<GetCategoryResult>> HandleAsync(GetCategories query)
        {
            return await _business.GetAll(query);
        }
    }
}
