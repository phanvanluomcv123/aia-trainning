using System.Threading.Tasks;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.DTO.Requests;
using AIA.Training.Core.DTO.Results;
using AutoMapper;
using OpenCqrs.Queries;

namespace AIA.Training.QueryHandler
{
    public class NotificationQueryHandler : IQueryHandlerAsync<GetNotificationQuery, ListResult<NotificationResult>>
    {
        private readonly INotificationBusiness _notificationBusiness;
        private readonly IMapper _mapper;

        public NotificationQueryHandler(INotificationBusiness notificationBusiness, IMapper mapper)
        {
            _notificationBusiness = notificationBusiness;
            _mapper = mapper;
        }

        public async Task<ListResult<NotificationResult>> RetrieveAsync(GetNotificationQuery query)
        {
            return await _notificationBusiness.GetAll(_mapper.Map<GetNotificationQuery>(query));
        }
    }
}
