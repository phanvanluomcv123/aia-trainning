using AIA.One.Core.Cryptography.Extensions;
using AIA.One.Core.Data;
using AIA.One.Core.DependencyInjection;
using AIA.One.Core.Jobs;
using AIA.One.Core.Security.Filters;
using AIA.One.Core.Security.Permissions;
using AIA.One.Core.Security.Roles;
using AIA.One.Core.UnitOfWork;
using AIA.One.Core.Web.Authorization;
using AIA.Training.Configs;
using AIA.Training.Core.Configs;
using AIA.Training.Core.Extensions;
using AIA.Training.Data;
using AIA.Training.Handlers;
using AIA.Training.Middlewares;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Hangfire;
using Hangfire.Dashboard;
using Kledex.Bus;
using Kledex.Extensions;
using Kledex.Store.EF.InMemory.Extensions;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using AIA.One.SDKs;
using Kledex.Validation.FluentValidation.Extensions;

namespace AIA.Training
{
    /// <summary>
    /// App startup for aspnet core 3.1 and 5
    /// </summary>
    public class Startup
    {
        private readonly string _envName;
        private readonly bool _isDevelopment;
        //private static HangfireConfig _hangfireConfig = new HangfireConfig();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="environment"></param>
        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            _isDevelopment = environment.IsDevelopment();
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                var logger = AppLoggerConfigurationExtension.CreateAndConfigureLogger(Configuration, "Serilog","logger.json");
                Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(logger).CreateLogger();
                ConfigAppSettings(services);

                // Configure for services only
                services.Configure<CookiePolicyOptions>(options =>
                {
                    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });

                services.AddScoped<JsonDefaultNamingStrategyAttribute>();

                ConfigureApplicationInsightsService(services);

                services.AddControllers().AddNewtonsoftJson();

                services.AddMemoryCache();
                services.AddJwtAuthentication(Configuration);
                services.AddSwaggerDocumentation();
                //services.AddEventGrid();
                //services.AddAppCachingWithMemoryOrRedis(AppSettings.Instance);

                WireUpInfrastructureServices(services);

                //AIA.One.SDKs.ExcelUtils.InitLicense();

                //_hangfireConfig.Init(
                //    AppSettings.Instance.BackgroundJobType,
                //    AppSettings.Instance.BackgroundJobConnection,
                //services);

                //init license for Aspose Cells
                ExcelUtils.InitLicense();
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Startup error");
                throw;
            }
        }

        /// <summary>
        /// ConfigureContainer is where you can register things directly
        /// with Autofac. This runs after ConfigureServices so the things
        /// here will override registrations made in ConfigureServices.
        /// Don't build the container; that gets done for you by the factory.
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            // Register autofac
            var container = WireUp(builder);

            // Register mapper
            var mapper = MapperConfig.Init();
            container.RegisterInstance<IMapper>(mapper);
            //container.RegisterPerLifetime<IBackgroundJob, AIA.One.Core.Jobs.HangfireJob>();
        }


        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            AppSettings.Instance.ApiContentRootPath = env.ContentRootPath;

            if (_isDevelopment)
            {
                app.UseDeveloperExceptionPage();
            }

            app.ExceptionConfiguration(Log.Logger);

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            //app.UseHangfireDashboard(options: new DashboardOptions
            //{
            //    PrefixPath = AppSettings.Instance.BaseUrl,
            //    IsReadOnlyFunc = (_) => true,
            //    Authorization = new IDashboardAuthorizationFilter[]
            //    {
            //        new HangfireDashboardAuth()
            //    }
            //});

            app.UseRouting();

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseMiddleware<RequestLoggingMiddleware>();

            app.UseKledex();

            app.UseSwaggerDocumentation();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapControllerRoute(
                    name: "api",
                    pattern: "{controller}/{id?}"
                );
            });

            //enable auto migraiton
            //using(var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            //{
            //    var dbContext = scope.ServiceProvider.GetService<DbContext>();
            //    dbContext.Database.Migrate();
            //}
        }

        private void ConfigAppSettings(IServiceCollection services)
        {
            ConfigAppSettingsJsonFile(services);

            Log.Logger.Information("Init keyvault");

            Core.Extensions.KeyVaultExtension.InitVault(AppSettings.Instance.AzureKeyVault.ClientId, AppSettings.Instance.AzureKeyVault.ClientSecret, AppSettings.Instance.AzureKeyVault.Name);
            AppSettings.Instance.ApplyVault();
            //JwtSettings.Instance.ApplyVault();
            AuthenticationExtensions.GetOpenIdConnectConfiguration();


            Log.Logger.Information(JsonConvert.SerializeObject(AppSettings.Instance));
            Log.Logger.Information(JsonConvert.SerializeObject(AuthenticationExtensions.OpenIdConnectConfiguration));
            services.AddSingleton<AppSettings>(AppSettings.Instance);

            //services.AddSingleton<OpenIdConnectConfiguration>(AuthenticationExtensions.OpenIdConnectConfiguration);
        }

        private void ConfigAppSettingsJsonFile(IServiceCollection services)
        {
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            //var jwtSettingsSection = Configuration.GetSection("JwtSettings");
            //services.Configure<JwtSettings>(jwtSettingsSection);

            var eventGridSection = Configuration.GetSection("EventGrid");
            var eventHubSection = Configuration.GetSection("EventHub");

            // configure jwt authentication
            AppSettings.Instance = appSettingsSection.Get<AppSettings>() ?? new AppSettings();
            //AppSettings.Instance.EventGrid = (AppSettings.Instance.EventGrid ?? eventGridSection.Get<EventGridSetting>()) ?? new EventGridSetting();
            //AppSettings.Instance.EventHub = (AppSettings.Instance.EventHub ?? eventHubSection.Get<EventHubSetting>()) ?? new EventHubSetting();
            //try
            //{
            //    var redisSection = appSettingsSection.GetSection("Redis");
            //    services.Configure<RedisSetting>(redisSection);
            //    AppSettings.Instance.Redis = (AppSettings.Instance.Redis ?? redisSection.Get<RedisSetting>()) ?? new RedisSetting();

            //}
            //catch (Exception ex)
            //{
            //    Log.Logger.Error(ex, ex.Message);
            //}
            //JwtSettings.Instance = jwtSettingsSection.Get<JwtSettings>() ?? new JwtSettings();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        private CoreContainerAdapter WireUp(ContainerBuilder builder)
        {
            // Autofac container adapter
            var container = new CoreContainerAdapter(builder);

            container.RegisterInstance<IRegisterDependencies>(container);
            container.RegisterInstance<IResolveDependencies>(container);
            container.RegisterInstance<ILogger>(Log.Logger);
            container.RegisterPerLifetime<IUnitOfWork, UnitOfWork>();
            new Data.Bootstrapper(container).WireUp();
            new Business.Bootstrapper(container).WireUp();
            // Validator
            new Validators.Bootstrapper(container).WireUp();

            // Infra
            //new Infrastructure.Bootstrapper(container, Log.Logger).WireUp();

            //Business
            new Business.ManualBootstrapper(container).WireUp();

            DependencyResolver.SetResolver(container);

            builder.RegisterBuildCallback(autofacContainer =>
            {
                container.SetContainerInstance(autofacContainer as IContainer);
            });

            return container;
        }

        private void WireUpInfrastructureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddDbContext<DbContext, CoreDataContext>(o => 
                o.UseSqlServer(AppSettings.Instance.CoreDb, sqlOptions => sqlOptions.MigrationsAssembly("AIA.Training"))
                .EnableSensitiveDataLogging());

            //services.AddSingleton<IAuthorizationHandler, DeviceIdHandler>();
            services.AddSingleton<IAuthorizationHandler, RolePermissionHandler>();
            services.AddSingleton<IAuthorizationPolicyProvider, AIAAuthorizationProvider>();
            services.AddSingleton<AIA.One.Core.Security.IAuthorizationService, RoleBaseAuthorizationService>();
            services.AddSingleton<IRoleService, RoleService>();
            services.AddSingleton<IPermissionProvider, PermissionStore>();

            services.AddKledex(typeof(QueryHandler.Bootstrapper),
                    typeof(CommandHandler.Bootstrapper),
                    typeof(EventHandler.Bootstrapper),
                    typeof(Validators.Bootstrapper))
                .AddFluentValidation(options => options.ValidateAllCommands = false)
                .AddInMemoryStore();
        }

        private void ConfigureApplicationInsightsService(IServiceCollection services)
        {
            try
            {
                services.AddAndConfigureApplicationInsightsService(AppSettings.Instance);
            }
            catch (Exception e)
            {
                Log.Logger.Warning($"Failed to configure the application insights service -> {e}");
            }
        }
    }
}
