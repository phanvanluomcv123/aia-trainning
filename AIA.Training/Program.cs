using System;
using System.IO;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace AIA.Training
{
    /// <summary>
    /// Web app entry point class
    /// </summary>
    public class Program
    {
        const string ASPNETCORE_URLS = "ASPNETCORE_URLS";

        /// <summary>
        /// Application configuration
        /// </summary>
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true, reloadOnChange: true)
            .AddJsonFile($".\\appsettings\\appsettings.{Environment.MachineName.ToLower()}.json", optional: true, reloadOnChange: true)
            .AddEnvironmentVariables()
            .Build();

        /// <summary>
        /// Web app entry point method
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static async Task Main(string[] args)
        {
            //Log.Logger = new LoggerConfiguration()
            //                .ReadFrom.Configuration(Configuration)
            //                .CreateLogger();

            try
            {
                Log.Information("Starting web host - training");

                Console.WriteLine("Starting web host - training");

                var host = CreateHostBuilder(args).Build();

                Log.Information($"Web app is starting at {Environment.GetEnvironmentVariable(ASPNETCORE_URLS)}");

                Console.WriteLine($"Web app is starting at {Environment.GetEnvironmentVariable(ASPNETCORE_URLS)}");

                await host.RunAsync();

            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");

                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        /// <summary>
        /// Create a host builder for web app
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var url = Environment.GetEnvironmentVariable(ASPNETCORE_URLS);

            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.CaptureStartupErrors(true);
                    webBuilder.UseSetting(WebHostDefaults.PreventHostingStartupKey, "true");
                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.UseConfiguration(Configuration);
                    webBuilder.UseUrls(url);
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseKestrel(option => {
                        option.Limits.MaxRequestBodySize = null;
                    });
                });
        }
    }
}
