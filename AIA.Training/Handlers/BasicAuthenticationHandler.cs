using AIA.Training.Core.Configs;
using AIA.Training.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace AIA.Training.Handlers
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IConfiguration config;
        private readonly IMemoryCache memCache;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock,
            IConfiguration config, IMemoryCache memCache)
            : base(options, logger, encoder, clock)
        {
            this.config = config;
            this.memCache = memCache;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return AuthenticateResult.Fail("Missing Authorization Header");
            }

            string cacheKey, username, password;
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                cacheKey = authHeader.Parameter;
                var credentialBytes = Convert.FromBase64String(cacheKey);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                username = credentials[0];
                password = credentials[1];
            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Authorization Header");
            }

            var authorities = new List<string>();

            if (!memCache.TryGetValue(cacheKey, out authorities))
            {
                try
                {
                    using (var client = new HttpClient())
                    {
                        var plainText = $"{AppSettings.Instance.AppClientId}:{AppSettings.Instance.AppClientSecret}";
                        var base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                        var url = $"{AppSettings.Instance.MdmApiUrl}/api/authorities/getByApp?appKey={WebUtility.UrlEncode(username)}&appSecret={WebUtility.UrlEncode(password)}";
                        var result = client.GetAsync(url).Result;
                        if (!result.IsSuccessStatusCode)
                        {
                            return AuthenticateResult.Fail("Invalid Authorization");
                        }
                        authorities = result.Content.ReadAsAsync<List<string>>().Result?.Distinct().ToList();
                    }

                    memCache.Set(cacheKey, authorities, TimeSpan.FromMinutes(10));
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"GetAuthorities error: {ex}");
                    return AuthenticateResult.Fail("GetAuthorities error");
                }

            }

            var claims = new List<Claim> {
                new Claim(ClaimTypes.NameIdentifier, username)
            };
            claims.AddRange(authorities.Select(m => new Claim(ClaimTypes.Role, m)));

            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            Console.WriteLine("BasicAuth success");
            return AuthenticateResult.Success(ticket);
        }
    }
}
