﻿using System.Net;
using System.Threading.Tasks;
using AIA.Training.Core.Results;
using AIA.Training.Core.Queries;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Configs;
using AIA.One.Core.Web.Authorization;
using Microsoft.AspNetCore.Authorization;
using AIA.One.Core.CQRS.Models;
using AIA.One.Core.CQRS.Exceptions;
using AIA.One.Core.Web.Controllers;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Kledex;
using Serilog;
using System.Linq;
using FluentValidation.Results;

namespace AIA.Training.APIs
{
    [Route("api/v1/student")]
    [AllowAnonymous]
    public partial class StudentController : BaseController
    {
        private readonly IDispatcher _dispatcher;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        public StudentController(IDispatcher dispatcher, ILogger logger, IMapper mapper)
        {
            _dispatcher = dispatcher;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("get-all")]
        [ProducesResponseType(typeof(ListResult<GetCategoryResult>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetStudents([FromBody] GetStudent request)
        {
			if (!ModelState.IsValid)
            {
                throw new InvalidValidationException(ModelState.Values.SelectMany(p => p.Errors.Select(e => new ValidationFailure("", e.ErrorMessage))).ToList());
            }
			request.Roles = Identity.Roles;
            request.Username = Identity.Username;
            var result = await _dispatcher.GetResultAsync(request);
            return Ok(result);
		}
	}
}
