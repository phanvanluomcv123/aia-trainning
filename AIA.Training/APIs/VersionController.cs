using AIA.One.Core.Web.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AIA.Training.APIs
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : ControllerBase
    {
        public IActionResult Check()
        {
            return Ok("ping");
        }
    }
}
