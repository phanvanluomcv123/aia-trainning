using AIA.One.Core.CQRS.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Serilog;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AIA.Training.Configs
{
    public static class ExceptionHandler
    {
        public static void ExceptionConfiguration(this IApplicationBuilder builder, ILogger logger)
        {
            builder.UseExceptionHandler(appError =>
            {
                appError.Run(async context =>
                {
                    if (context.Response.StatusCode == (int)HttpStatusCode.InternalServerError)
                    {
                        context.Response.ContentType = "application/json";
                        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                        if (contextFeature.Error is InvalidValidationException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            await context.Response.WriteAsync(contextFeature.Error.ToString());
                        }
                        else
                        {
                            var guidId = Guid.NewGuid().ToString();
                            logger.Error($"{guidId}_{contextFeature.Error}");
                            await context.Response.WriteAsync($"{{\"Error\": \"System encounted errors, please contact administrator with code: {guidId}\"}}");
                        }
                    }
                });
            });            
        }
    }
}
