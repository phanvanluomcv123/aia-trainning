using AIA.One.Core.Security;
using AIA.Training.Core.Configs;
using AIA.Training.Extensions;
using AIA.Training.Handlers;
using AIA.Training.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Threading;
using Microsoft.IdentityModel.Protocols;

namespace AIA.Training.Configs
{
    public static class AuthenticationExtensions
    {
        public static OpenIdConnectConfiguration OpenIdConnectConfiguration;

        public static void GetOpenIdConnectConfiguration()
        {
            var configManager = new ConfigurationManager<OpenIdConnectConfiguration>(AppSettings.Instance.IssuerEnpoint,
                new OpenIdConnectConfigurationRetriever());
            OpenIdConnectConfiguration =  configManager.GetConfigurationAsync().Result;
        }

        public static IServiceCollection AddBasicAuthentication(this IServiceCollection services)
        {
            services
               .AddAuthentication("BasicAuthentication")
                   .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

            return services;
        }

        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, IConfiguration config)
        {
            IdentityModelEventSource.ShowPII = true;
            services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKeys = OpenIdConnectConfiguration.SigningKeys,
                    ValidIssuer = OpenIdConnectConfiguration.Issuer,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateLifetime = true,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.FromMinutes(15)
                };
                o.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async ctx =>
                    {
                        var memCache = ctx.HttpContext.RequestServices.GetRequiredService<IMemoryCache>();
                        var claimsIdentity = ctx.Principal.Identity as ClaimsIdentity;
                        var token = (JwtSecurityToken)ctx.SecurityToken;
                        var oid = token.Payload["oid"];
                        if (!memCache.TryGetValue(oid, out Profile.Info profileInfo))
                        {
                            profileInfo = await GetProfileInfo(token.RawData);
                            if (profileInfo == null)
                            {
                                return;
                            }

                            memCache.Set(oid, profileInfo, TimeSpan.FromHours(1));
                        }

                        var authorities = new List<string>();
                        foreach (var item in profileInfo.role)
                        {
                            claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, item.name));
                            authorities.AddRange(item.permissions.Select(p => p.name).ToList());
                        }

                        claimsIdentity.AddClaims(authorities.Distinct().Select(m => new Claim(AIAClaimTypes.Permission, m)));

                        await Task.CompletedTask;
                    }
                };
            });

            return services;
        }

        private static RsaSecurityKey SigningKey(string key, string expo)
        {
            RSA rrr = RSA.Create();

            rrr.ImportParameters(
                new RSAParameters()
                {
                    Modulus = Base64UrlEncoder.DecodeBytes(key),
                    Exponent = Base64UrlEncoder.DecodeBytes(expo)
                }
            );

            return new RsaSecurityKey(rrr);
        }

        private async static Task<Profile.Info> GetProfileInfo(string token)
        {
            var profileInfo = new Profile.Info();
            using (var client = new HttpClient
            {
                BaseAddress = new Uri(AppSettings.Instance.MdmApiUrl),
            })
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var root = AppSettings.Instance.MdmApiUrl.TrimEnd('/');
                var result = await client.GetAsync($"{root}/api/users/my-full-profile");
                profileInfo = await result.Content.ReadAsAsync<Profile.Info>();
            }

            return profileInfo;
        }
    }
}
