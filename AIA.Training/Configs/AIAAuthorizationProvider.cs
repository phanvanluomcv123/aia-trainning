using AIA.One.Core.Web.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIA.Training.Configs
{
    public class AIAAuthorizationProvider : AuthorizationPolicyProvider
    {
        public AIAAuthorizationProvider(IOptions<AuthorizationOptions> options) : base(options)
        {
        }

        public override Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (policyName.StartsWith(DeviceCheckAttribute.PolicyPrefix, StringComparison.OrdinalIgnoreCase))
            {
                return Task.FromResult(new AuthorizationPolicyBuilder().AddRequirements(new DeviceIdRequirement()).Build());
            }

            return base.GetPolicyAsync(policyName);
        }
    }
}
