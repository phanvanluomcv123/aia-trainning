using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIA.Training.Configs
{
    public class DeviceCheckAttribute : AuthorizeAttribute
    {
        public const string PolicyPrefix = "AIAONE.DEVICEID";

        public DeviceCheckAttribute()
        {
            Policy = $"{PolicyPrefix}";
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
    }
}
