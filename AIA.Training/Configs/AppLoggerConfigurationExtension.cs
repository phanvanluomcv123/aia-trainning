using AIA.Training.Core.Models.CustomModel;
using Dazinator.AspNet.Extensions.FileProviders;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;

namespace AIA.Training.Configs
{
    /// <summary>
    /// Create and configure serilog logger instance for application
    /// </summary>
    public static class AppLoggerConfigurationExtension
    {
        /// <summary>
        /// Create and configure serilog logger instance for application
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="appSettings"></param>
        /// <returns></returns>
        public static IConfigurationRoot CreateAndConfigureLogger(IConfiguration configuration, string section, string filename)
        {
            var logModel = configuration.GetSection(section).Get<LogModel>();
            logModel.WriteTo[1].Args.Path = string.Format(logModel.WriteTo[1].Args.Path, DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            var provider = new InMemoryFileProvider();
            provider.Directory.AddFile("/", new StringFileInfo(JsonConvert.SerializeObject(new { Serilog = logModel }), filename));
            var loggerConfig = new ConfigurationBuilder().AddJsonFile(provider, filename, false, false).Build();
            return loggerConfig;
        }
    }
}
