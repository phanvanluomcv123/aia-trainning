using AIA.Training.Core.Configs;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AIA.Training.Configs
{
    /// <summary>
    /// Provide extension method to configure AI services for the web app
    /// </summary>
    public static class ApplicationInsightsConfigureServicesExtension
    {
        /// <summary>
        /// Configure the AI service with specific connection string and service options
        /// https://docs.microsoft.com/en-us/azure/azure-monitor/app/asp-net-core
        /// </summary>
        /// <param name="services"></param>
        /// <param name="appSettings"></param>
        public static void AddAndConfigureApplicationInsightsService(this IServiceCollection services, AppSettings appSettings)
        {
            if (appSettings == null || string.IsNullOrEmpty(appSettings.ApplicationInsightsConnectionString))
            {
                // Try to load the configuration from ApplicationInsights section
                services.AddApplicationInsightsTelemetry();
            }
            else
            {
                var aiOptions = new ApplicationInsightsServiceOptions
                {
                    ConnectionString = appSettings.ApplicationInsightsConnectionString
                };
                services.AddApplicationInsightsTelemetry(aiOptions);
            }
        }
    }
}
