using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Buffers;

namespace AIA.Training.Configs
{
    public class JsonDefaultNamingStrategyAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is ObjectResult objectResult)
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver()
                };

                var jsonFormatter = new NewtonsoftJsonOutputFormatter(
                    serializerSettings,
                    ArrayPool<char>.Shared,
                    new MvcOptions());

                objectResult.Formatters.Add(jsonFormatter);
            }

            base.OnResultExecuting(context);
        }
    }
}
