using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIA.Training.Configs
{
    public class DeviceIdRequirement : IAuthorizationRequirement
    {
        public string DeviceId { get; }

        public DeviceIdRequirement()
        {
            DeviceId = "";
        }
    }
}
