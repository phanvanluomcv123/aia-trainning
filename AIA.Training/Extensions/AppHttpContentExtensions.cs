using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace AIA.Training.Extensions
{
    /// <summary>
    /// Application own extension methods for http stuff
    /// </summary>
    public static class AppHttpContentExtensions
    {
        /// <summary>
        /// Extension method for replacement of ReadAsAsync when moving from .net core 2.x to 3.x
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="content"></param>
        /// <returns></returns>
        public static async Task<T> ReadAsAsync<T>(this HttpContent content) =>
            JsonConvert.DeserializeObject<T>(await content.ReadAsStringAsync());
    }

}
