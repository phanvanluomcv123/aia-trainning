using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIA.Training.Models
{
    public class Profile
    {
        public class Permission
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public class Role
        {
            public int id { get; set; }
            public string name { get; set; }
            public List<Permission> permissions { get; set; }
        }

        public class Info
        {
            public string username { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public object idNumber { get; set; }
            public object address { get; set; }
            public object birthday { get; set; }
            public string fullName { get; set; }
            public string azureUid { get; set; }
            public List<Role> role { get; set; }
            public string createdBy { get; set; }
            public DateTime createdAt { get; set; }
            public string updatedBy { get; set; }
            public DateTime updatedAt { get; set; }
        }
    }
}
