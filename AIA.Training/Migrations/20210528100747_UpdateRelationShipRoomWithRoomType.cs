﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AIA.Training.Migrations
{
    public partial class UpdateRelationShipRoomWithRoomType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Room_RoomStatus_RoomStatusId",
                table: "Room");

            migrationBuilder.DropForeignKey(
                name: "FK_Room_RoomType_RoomTypeId",
                table: "Room");

            migrationBuilder.DropIndex(
                name: "IX_RoomType_RoomId",
                table: "RoomType");

            migrationBuilder.DropIndex(
                name: "IX_RoomStatus_RoomId",
                table: "RoomStatus");

            migrationBuilder.DropIndex(
                name: "IX_Room_RoomStatusId",
                table: "Room");

            migrationBuilder.DropIndex(
                name: "IX_Room_RoomTypeId",
                table: "Room");

            migrationBuilder.DropColumn(
                name: "RoomStatusId",
                table: "Room");

            migrationBuilder.DropColumn(
                name: "RoomTypeId",
                table: "Room");

            migrationBuilder.CreateIndex(
                name: "IX_RoomType_RoomId",
                table: "RoomType",
                column: "RoomId",
                unique: true,
                filter: "[RoomId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_RoomStatus_RoomId",
                table: "RoomStatus",
                column: "RoomId",
                unique: true,
                filter: "[RoomId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_RoomType_RoomId",
                table: "RoomType");

            migrationBuilder.DropIndex(
                name: "IX_RoomStatus_RoomId",
                table: "RoomStatus");

            migrationBuilder.AddColumn<string>(
                name: "RoomStatusId",
                table: "Room",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RoomTypeId",
                table: "Room",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoomType_RoomId",
                table: "RoomType",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_RoomStatus_RoomId",
                table: "RoomStatus",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_RoomStatusId",
                table: "Room",
                column: "RoomStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Room_RoomTypeId",
                table: "Room",
                column: "RoomTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Room_RoomStatus_RoomStatusId",
                table: "Room",
                column: "RoomStatusId",
                principalTable: "RoomStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Room_RoomType_RoomTypeId",
                table: "Room",
                column: "RoomTypeId",
                principalTable: "RoomType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
