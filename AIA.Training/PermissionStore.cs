using AIA.One.Core.Security.Permissions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AIA.Training
{
    public class PermissionStore : IPermissionProvider
    {
        public string Name => "IPOSV2";

        public const string ContactCreate = "DP_CUSTOMER_CARE",
                                ContactView = "DP_SALE_REPORT_4";


        public PermissionModule GetPermissionModule()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Permission> GetPermissions()
        {
            var result = new List<Permission>();

            var contactView = new Permission
            {
                Name = ContactView
            };

            var contactCreate = new Permission
            {
                Name = ContactCreate,
                ImpliedBy = new List<Permission>
                {
                    contactView
                }
            };

            return new List<Permission>
            {
                contactCreate
            };
        }
    }
}
