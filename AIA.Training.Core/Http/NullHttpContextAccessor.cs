using Microsoft.AspNetCore.Http;
using System;

namespace AIA.Training.Core.Http
{
    public class NullHttpContextAccessor : IHttpContextAccessor
    {
        public HttpContext HttpContext
        {
            get => null;
            set => throw new NotImplementedException("Setting value to HttpContext of NullHttpContextAccessor is not supported");
        }
    }
}
