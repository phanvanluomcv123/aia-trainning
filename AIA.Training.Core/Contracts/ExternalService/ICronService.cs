using System.Threading;
using System.Threading.Tasks;

namespace AIA.Training.Core.Contracts.ExternalService
{
    public interface ICronService
    {
        Task RegisterAsync(CancellationToken token);
        Task RemoveAsync(CancellationToken token);
        void Execute();
    }
}
