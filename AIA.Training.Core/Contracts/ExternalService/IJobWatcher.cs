using System.Collections.Generic;

namespace AIA.Training.Core.Contracts.ExternalService
{
    public interface IJobWatcher
    {
        List<string> RunningJobs { get; set; }
    }
}
