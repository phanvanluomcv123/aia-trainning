using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AIA.One.Core.CQRS.Models;
using System.Threading.Tasks;
using AIA.Training.Core.Models;

namespace AIA.Training.Core.Contracts.Business
{
    public partial interface ICategoryBusiness
    {
        Task<GetCategoryResult> GetDetail(GetCategory query);
        Task<ListResult<GetCategoryResult>> GetAll(GetCategories query);
        Task CreateOrUpdate(CreateOrUpdateCategory command);
        bool IsCategoryNameExist(string name, string exceptCategoryId = null);
    }
}
