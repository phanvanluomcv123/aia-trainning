﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AIA.One.Core.CQRS.Models;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;

namespace AIA.Training.Core.Contracts.Business
{
    public partial interface IRoomBusiness
    {
        Task<GetRoomResult> GetRoomDetail(GetRoom query);
        Task<ListResult<GetRoomResult>> GetAll(GetRooms query);
        Task CreateOrUpdate(CreateOrUpdateRoom command);
        Task DeleteRoom(DeleteRoom command);
    }
}
