using AIA.One.Core.Cryptography;
using System;

namespace AIA.Training.Core.Configs
{
    public class AppSettings
    {
        public static AppSettings Instance { get; set; }

        public string ApiContentRootPath { get; set; }

        public string Version { get; set; }

        public string MdmApiUrl { get; set; }

        public string AppEnv { get; set; } = AppEnviroment.UAT.ToString();

        //[KeyVault]
        public string AppClientId { get; set; }

        //[KeyVault]
        public string AppClientSecret { get; set; }

        public string TemplateFolder { get; set; }

        public string OutputFolder { get; set; }

        public string FontPath { get; set; }

        //[KeyVault]
        public string CoreDb { get; set; }

        public string BaseUrl { get; set; }

        public string BackgroundJobType { get; set; }
        public string BackgroundJobConnection { get; set; }

        //[KeyVault]
        public string OracleMDMDb { get; set; }

        public string LeadUploadFolder { get; set; }

        public string MoDMInventoryUploadFolder { get; set; }

        public string MoDMHashKey { get; set; } = "G0EVOvh1XetaYTizx0I";

        public int? ClockSkew { get; set; }

        public EventGridSetting EventGrid { get; set; }
        public EventHubSetting EventHub { get; set; }
        public AzureStorageSetting AzureStorage { get; set; } = new AzureStorageSetting();
        public AzureKeyVaultSettings AzureKeyVault { get; set; }
        public string MoDMKey { get; set; }
        public string MoDMUrl { get; set; }

        public AppPartySetting AppParty { get; set; } = new AppPartySetting();

        public string BackgroundJobServiceId { get; set; }

        //public RedisSetting Redis { get; set; }

        //[KeyVault]
        public string ApplicationInsightsConnectionString { get; set; }

        public string IssuerEnpoint { get; set; }

        public AppSettings()
        {
            EventGrid = new EventGridSetting();
            EventHub = new EventHubSetting();
            AzureKeyVault = new AzureKeyVaultSettings();
            BackgroundJobServiceId = Guid.NewGuid().ToString();
            //Redis = new RedisSetting();
        }
    }
}
