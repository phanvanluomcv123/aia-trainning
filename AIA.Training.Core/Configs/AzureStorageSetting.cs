using AIA.One.Core.Cryptography;

namespace AIA.Training.Core.Configs
{
    public class AzureStorageSetting
    {
        //[KeyVault]
        public string BlobConnectionString { get; set; }
        public string BlobContainerName { get; set; }

        public string DocumentContainerName { get; set; }
        public string DocumentSubFolder { get; set; }
    }
}
