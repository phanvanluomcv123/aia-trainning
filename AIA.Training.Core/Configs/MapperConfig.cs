using System;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.Models;
using AIA.Training.Core.Queries;
using AIA.Training.Core.Results;
using AutoMapper;
using AutoMapper.Configuration;

namespace AIA.Training.Core.Configs
{
    public class MapperConfig
    {
        public static IMapper Init()
        {
            var cfg = new MapperConfigurationExpression();

            cfg.CreateMap<Category, GetCategory>();
            cfg.CreateMap<Category, GetCategoryResult>();
            cfg.CreateMap<Category, CreateOrUpdateCategory>();
            cfg.CreateMap<CreateOrUpdateCategory, Category>().ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            cfg.CreateMap<Student,GetStudentResult>().ForMember(dest => dest.Name,from => from.MapFrom(src => src.Name));
            cfg.CreateMap<Room, GetRoom>();
            cfg.CreateMap<Room, GetRoomResult>();
            cfg.CreateMap<CreateOrUpdateRoom, Room>();
            var mapperConfig = new MapperConfiguration(cfg);
            return new Mapper(mapperConfig);
        }
    }
}
