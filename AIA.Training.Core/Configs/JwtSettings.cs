
using AIA.One.Core.Cryptography;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;

namespace AIA.Training.Core.Configs
{
    public class JwtSettings
    {
        public static JwtSettings Instance { get; set; }

        public OpenIdConnectConfiguration OpenIdConfiguration { get; set; }
    }
}
