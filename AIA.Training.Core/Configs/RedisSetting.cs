using AIA.One.Core.Cryptography;

namespace AIA.Training.Core.Configs
{
    public class RedisSetting
    {
        public string AppCacheKeyPrefix { get; set; } = "AIA.Training";

        public bool RedisEnabled { get; set; }

        public bool UseRedisForCaching { get; set; }

        public bool IgnoreRedisTimeoutException { get; set; }

        /// <summary>
        /// Default cache time in minute
        /// </summary>
        public int DefaultCacheTimeInMinute { get; set; }

        /// <summary>
        /// Short duration cache time in minute
        /// </summary>
        public int ShortTermCacheTimeInMinute { get; set; }

        public int? RedisDatabaseId { get; set; }

        //[KeyVault]
        public string RedisConnectionString { get; set; }
    }
}
