namespace AIA.Training.Core.Configs
{
    public class AzureKeyVaultSettings
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Name { get; set; }
     }

}