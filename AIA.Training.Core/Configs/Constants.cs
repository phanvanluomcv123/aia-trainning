using System;
using System.Collections.Generic;

namespace AIA.Training.Core.Configs
{
    public class Constants
    {
        public const string DATE_IMPORT_FORMAT = "dd/MM/yyyy";
        public const int DEFAULT_PAGE_SIZE = 20;
        public const int DEFAULT_PAGE_INDEX = 1;

       
    }
    public class JobCode
    {
        public const string Demo_Job_Code = "DEMO_JOB_CODE";
    }

    public class CourseConstants
    {
        public enum CourseStatus
        {
            //Soan thao
            Draft = 0,
            //Huy
            Cancelled = 1,
            //Doi duyet
            WaitingForApproval = 2,
            //Da duyet
            Approved = 3,
            //Tu Choi
            Rejected = 4
        }

        public enum Type
        {
            //Non-product
            N = 0,
            //Product
            P = 1
        }
    }

    public class DocumentConstants
    {
        public const string _DocumentFolder = "Document";
        public const string _ExamFolder = "ExamResult";
        public enum DocumentType
        {
            Document = 0,
            Scorm = 1,
            ExamResult = 2,
            Avatar = 3
        }
    }

}
