namespace AIA.Training.Core.Configs
{
    public class EventHubSetting
    {
        public string HubConnection { get; set; }
        public string HubNamespace { get; set; }
        public string StoreConnection { get; set; }
        public string StoreContainer { get; set; }
        public int PartitionCount { get; set; }
    }
}