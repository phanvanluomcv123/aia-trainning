namespace AIA.Training.Core.Configs
{
    public class AppPartySettings
    {
        public string AndroidDefaultAppDownloadLink { get; set; }
        public string IosDefaultAppDownloadLink { get; set; }
        public string ApiUrl { get; set; } = "https://app-party.azurewebsites.net/api";
        public string Key { get; set; } = "doi/NIjrAX/6o4V4WNCscf/TAekjaWYlLVD/MIFj9RTUYx2waPBhpw==";
        public string AuthenAttribute { get; set; } = "X-Functions-Key";
        public string Env { get; set; } = "UAT";
    }
}
