using AIA.One.Core.Cryptography;

namespace AIA.Training.Core.Configs
{
    public class EventGridSetting
    {
        //[KeyVault]
        public string TopicEndpoint { get; set; }

        //[KeyVault]
        public string TopicKey { get; set; }

        public string DomainEndpoint { get; set; }
        public string DomainKey { get; set; }
    }
}