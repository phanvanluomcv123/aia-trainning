using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AIA.One.Core.CQRS.Models;
using System.ComponentModel.DataAnnotations;
using AIA.Training.Core.Configs;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.CQRS.Queries.CustomModel;
using AIA.Training.Core.CQRS.Results.CustomModel;
namespace AIA.Training.Core.Results
{
	public partial class GetClassResult 
	{
		public string Id { get; set; }

		public bool Deleted { get; set; }

		public string Description { get; set; }

		public string Name { get; set; }

		public string CourseId { get; set; }

		public string Address { get; set; }

		public DateTime? StartDate { get; set; }

		public DateTime? EndDate { get; set; }

		public int? Slot { get; set; }

		public string Status { get; set; }

		public bool IsOnline { get; set; }

		public int? NumberOfRegisted { get; set; }

		public string CancelNote { get; set; }

		public DateTime? FinalDate { get; set; }

		public string TrainerId { get; set; }

		public string Offices { get; set; }

		public string MOFLocationId { get; set; }

		public string Tags { get; set; }

		public string ScormLink { get; set; }

	}
}
