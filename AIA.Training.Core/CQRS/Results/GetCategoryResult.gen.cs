﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AIA.One.Core.CQRS.Models;
using System.ComponentModel.DataAnnotations;
using AIA.Training.Core.Configs;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.CQRS.Queries.CustomModel;
using AIA.Training.Core.CQRS.Results.CustomModel;
namespace AIA.Training.Core.Results
{
	public partial class GetCategoryResult 
	{
		public string Id { get; set; }

		public bool Deleted { get; set; }

		public string Description { get; set; }

		public string Name { get; set; }

		public string Type { get; set; }

	}
}
