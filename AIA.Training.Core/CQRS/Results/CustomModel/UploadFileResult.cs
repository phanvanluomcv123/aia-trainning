using System.Collections.Generic;

namespace AIA.Training.Core.CQRS.Results.CustomModel
{
    public class UploadFileResult
    {
        public List<FileDetailResult> FileResults { get; set; }

        public bool IsSuccess { get; set; }

        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class FileDetailResult
    {
        public string Path { get; set; }

        public string Id { get; set; }

        public string FileName { get; set; }
    }
}
