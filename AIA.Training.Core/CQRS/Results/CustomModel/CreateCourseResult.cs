using System.Collections.Generic;

namespace AIA.Training.Core.CQRS.Results.CustomModel
{
    public class CreateCourseResult
    {
        public string Id { get; set; }
    }

}
