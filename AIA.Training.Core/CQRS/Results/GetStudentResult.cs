﻿namespace AIA.Training.Core.Results
{
    public partial class GetStudentResult 
	{
		public string Id { get; set; }

		public string Name { get; set; }

		public int Age { get; set; }

	}
}
