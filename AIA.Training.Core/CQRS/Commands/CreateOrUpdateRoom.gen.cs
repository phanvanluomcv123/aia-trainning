﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AIA.One.Core.CQRS.Models;
using AIA.Training.Core.CQRS;
using System.ComponentModel.DataAnnotations;
using AIA.Training.Core.Configs;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.CQRS.Queries.CustomModel;
using AIA.Training.Core.CQRS.Results.CustomModel;

namespace AIA.Training.Core.CQRS.Commands
{
	public partial class CreateOrUpdateRoom : BaseCommand
	{
		public CreateOrUpdateRoom()
		{
			ValidateCommand = false;
		}

		[Required]
        public string RoomNumber { get; set; }
		[Required]
        public string Location { get; set; }
        public bool Deleted { get; set; }
        public string Id { get; set; }
	}
}
