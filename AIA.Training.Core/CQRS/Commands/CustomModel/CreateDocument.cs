using System;
using System.Collections.Generic;
using System.Text;
using AIA.Training.Core.Configs;

namespace AIA.Training.Core.CQRS.Commands.CustomModel
{
    public partial class CreateDocument
    {
        public string Path { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public DocumentConstants.DocumentType? Type { get; set; }
        public double? FileSize { get; set; }
        public int? Minutes { get; set; }
    }
}
