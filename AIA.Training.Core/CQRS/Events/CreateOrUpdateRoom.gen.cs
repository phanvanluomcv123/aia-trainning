﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AIA.One.Core.CQRS.Models;
using AIA.Training.Core.CQRS;
using AIA.Training.Core.Configs;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.CQRS.Queries.CustomModel;
using AIA.Training.Core.CQRS.Results.CustomModel;

namespace AIA.Training.Core.CQRS.Events
{
	public partial class CreateOrUpdateRoom : BaseEvent
	{
        public string RoomNumber { get; set; }
        public string Location { get; set; }
        public bool Deleted { get; set; }
        public string Id { get; set; }
	}
}
