﻿using AIA.One.Core.CQRS.Models;
using AIA.Training.Core.Results;
namespace AIA.Training.Core.Queries
{
    public partial class GetStudent : BaseQuery<ListResult<GetStudentResult>>
	{
        public string Search { get; set; }
	}
}
