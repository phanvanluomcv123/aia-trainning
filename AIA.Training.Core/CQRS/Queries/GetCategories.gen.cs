﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using AIA.One.Core.CQRS.Models;
using System.ComponentModel.DataAnnotations;
using AIA.Training.Core.Configs;
using AIA.Training.Core.CQRS.Commands.CustomModel;
using AIA.Training.Core.CQRS.Queries.CustomModel;
using AIA.Training.Core.CQRS.Results.CustomModel;
using AIA.Training.Core.Results;
namespace AIA.Training.Core.Queries
{
	public partial class GetCategories : BaseQuery<ListResult<GetCategoryResult>>
	{
		public bool? Deleted { get; set; }

		public string Name { get; set; }

	}
}
