using AIA.Training.Core.CQRS.Results.CustomModel;
using AIA.One.Core.CQRS.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using static AIA.Training.Core.Configs.DocumentConstants;

namespace AIA.Training.Core.CQRS.Queries.CustomModel
{
    public class UploadFile : BaseQuery<UploadFileResult>
    {
        public string CourseId { get; set; }
        public string Name { get; set; }
        public List<IFormFile> Files { get; set; }
        public int Minutes { get; set; }
        public DocumentType Type { get; set; }
        public string Description { get; set; }
    }
}
