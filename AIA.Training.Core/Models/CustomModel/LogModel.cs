using Newtonsoft.Json;
using System.Collections.Generic;

namespace AIA.Training.Core.Models.CustomModel
{
    public class Override
    {
        [JsonProperty("Microsoft")]
        public string Microsoft { get; set; }

        [JsonProperty("System")]
        public string System { get; set; }
    }

    public class MinimumLevel
    {
        [JsonProperty("Default")]
        public string Default { get; set; }

        [JsonProperty("Override")]
        public Override Override { get; set; }
    }

    public class Args
    {
        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("rollingInterval")]
        public string RollingInterval { get; set; }

        [JsonProperty("retainedFileCountLimit")]
        public string RetainedFileCountLimit { get; set; }

        [JsonProperty("rollOnFileSizeLimit")]
        public string RollOnFileSizeLimit { get; set; }

        [JsonProperty("shared")]
        public string Shared { get; set; }

        [JsonProperty("flushToDiskInterval")]
        public string FlushToDiskInterval { get; set; }
    }

    public class WriteTo
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Args")]
        public Args Args { get; set; }
    }

    public class LogModel
    {
        public MinimumLevel MinimumLevel { get; set; }
        public List<WriteTo> WriteTo { get; set; }
    }
}
