namespace AIA.Training.Core.Models.CustomModel
{
    public class BaseEntity : AIA.One.Core.BaseEntity
    {
        public virtual string Position { get; set; }
    }
}
