﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
    public partial class RoomStatus : BaseEntity
    {
        public RoomStatus()
        {
        }

        public RoomStatus(Guid id)
            : this()
        {
            Id = id.ToString();
        }

        public RoomStatus(string id)
            : this()
        {
            Id = id;
        }

        public override string TypeName => "RoomStatus";

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string RoomId { get; set; }

        [ForeignKey("RoomId")]
        public Room Room { get; set; }
    }

    public enum RoomStatusOrderBy
    {
        Name,
        RoomId,
        Room
    }
}