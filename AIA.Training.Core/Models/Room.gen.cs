﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
    public partial class Room : BaseEntity
    {
        public Room()
        {
        }

        public Room(Guid id)
            : this()
        {
            Id = id.ToString();
        }

        public Room(string id)
            : this()
        {
            Id = id;
        }

        public override string TypeName => "Room";

        [MaxLength(255)]
        public string RoomNumber { get; set; }

        [MaxLength(255)] public string Location { get; set; }

        public RoomType RoomType { get; set; }

        public RoomStatus RoomStatus { get; set; }
    }

    public enum RoomOrderBy
    {
        Name,
        Location,
        RoomTypeId,
        RoomType,
        RoomStatusId,
        RoomStatus
    }
}