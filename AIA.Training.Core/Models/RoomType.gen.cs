﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
    public partial class RoomType : BaseEntity
    {
        public RoomType()
        {
        }

        public RoomType(Guid id)
            : this()
        {
            Id = id.ToString();
        }

        public RoomType(string id)
            : this()
        {
            Id = id;
        }

        public override string TypeName => "RoomType";

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string RoomId { get; set; }

        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
    }

    public enum RoomTypeOrderBy
    {
        Name,
        RoomId,
        Room
    }
}