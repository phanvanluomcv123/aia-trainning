﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
	public partial class Category : BaseEntity
	{
		public override string TypeName
        {
            get
            {
                return "Category";
            }
        }

		[Required(AllowEmptyStrings=true)]
		[MaxLength(255)]
		public String Name { get; set; }
		[MaxLength(255)]
		public String Description { get; set; }
		[MaxLength(20)]
		public String Type { get; set; }

		public Category()
		{
		}

		public Category(Guid id)
		    :this()
		{
			Id = id.ToString();
		}

		public Category(string id)
		    :this()
		{
			Id = id;
		}
	}

	public enum CategoryOrderBy
	{
		Name,
		Description,
		Type
		
	}

}
