﻿using AIA.Training.Core.Contracts.ExternalService;
using System.Collections.Generic;

namespace AIA.Training.Core.Models
{
    public class JobWatcher : IJobWatcher
    {
        public List<string> RunningJobs { get; set; }

        public JobWatcher()
        {
            RunningJobs = new List<string>();
        }
    }
}
