﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
	public partial class Student : BaseEntity
	{
		public override string TypeName
        {
            get
            {
                return "Student";
            }
        }

		[Required(AllowEmptyStrings=true)]
		[MaxLength(255)]
		public String Name { get; set; }
		[MaxLength(255)]
		public String Description { get; set; }
		public DateTime? Birthday { get; set; }

		public Student()
		{
		}

		public Student(Guid id)
		    :this()
		{
			Id = id.ToString();
		}

		public Student(string id)
		    :this()
		{
			Id = id;
		}
	}

	public enum StudentOrderBy
	{
		Name,
		Description,
		Birthday
		
	}

}
