﻿using System;
using System.ComponentModel.DataAnnotations;
using AIA.Training.Core.Models.CustomModel;

namespace AIA.Training.Core.Models
{
    public partial class RoomService : BaseEntity
    {
        public RoomService()
        {
        }

        public RoomService(Guid id)
            : this()
        {
            Id = id.ToString();
        }

        public RoomService(string id)
            : this()
        {
            Id = id;
        }

        public override string TypeName => "RoomService";

        [MaxLength(255)]
        public string Name { get; set; }
    }

    public enum RoomServiceOrderBy
    {
        Name
    }
}