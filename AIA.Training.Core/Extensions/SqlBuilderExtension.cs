using DbExtensions;
using System.Collections.Generic;

namespace AIA.Training.Core.Extensions
{
    public static class SqlBuilderExtension
    {
        public static SqlBuilder ORDER_BY(this SqlBuilder @this, string propetyName, bool Desc, string acronymForTable = null)
        {
            string l = string.Empty;
            if (!string.IsNullOrEmpty(acronymForTable))
            {
                l = string.Concat(acronymForTable, ".");
            }

            if (Desc)
            {
                return @this.ORDER_BY($"{l}{ propetyName } DESC");
            }
            else
            {
                return @this.ORDER_BY($"{l}{ propetyName }");
            }
        }

        public static SqlBuilder PAGING(this SqlBuilder @this, Dictionary<string, object> mapParams, int PageIndex, int PageSize)
        {
            @this.AppendClause("OFFSET", null, "@Offset ROWS");
            @this.AppendClause("FETCH NEXT", null, " @FetchNext ROWS ONLY");
            mapParams.TryAdd("Offset", (PageIndex - 1) * PageSize);
            mapParams.TryAdd("FetchNext", PageSize);

            return @this;
        }
    }
}
