using System;
using System.Collections.Generic;
using System.Text;

namespace AIA.Training.Core.Extensions
{
    public static class StringExtension
    {
        public static string GetValueOrDefault(string str, string defaultvalue)
        {
            return string.IsNullOrEmpty(str) ? defaultvalue : str;
        }

        /// <summary>
        /// Returns the first part of the strings, up until the character c. If c is not found in the
        /// string the whole string is returned.
        /// </summary>
        /// <param name="s">String to truncate</param>
        /// <param name="c">Character to stop at.</param>
        /// <returns>Truncated string</returns>
        public static string LeftOf(this string s, char c)
        {
            int ndx = s.IndexOf(c);
            if (ndx >= 0)
            {
                return s.Substring(0, ndx);
            }

            return s;
        }

        /// <summary>
        /// Return the remainder of a string s after a separator c.
        /// </summary>
        /// <param name="s">String to search in.</param>
        /// <param name="c">Separator</param>
        /// <returns>The right part of the string after the character c, or the string itself when c isn't found.</returns>
        public static string RightOf(this string s, char c)
        {
            int ndx = s.IndexOf(c);
            if (ndx == -1)
                return s;
            return s.Substring(ndx + 1);
        }

        public static bool EqualsIgnoreCase(this string s, string other)
        {
            if (string.IsNullOrEmpty(s) && string.IsNullOrEmpty(other))
                return true;

            if (string.IsNullOrEmpty(s))
                return false;

            return string.Compare(s, other, ignoreCase: true) == 0;
        }
    }
}
