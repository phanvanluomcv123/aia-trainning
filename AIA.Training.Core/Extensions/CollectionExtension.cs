using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace AIA.Training.Core.Extensions
{
    public static class CollectionExtension
    {
        public static void BatchExecute<T>(this IEnumerable<T> source, int pageSize, Action<IEnumerable<T>> action)
        {
            var totalPage = Math.Ceiling(source.Count() * 1.0 / pageSize);

            for (int i = 0; i < totalPage; i++)
            {
                var skip = i * pageSize;
                var items = source.Skip(skip).Take(pageSize);
                action(items);
            }
            
        }

        public static void BatchExecute<T, TResult>(this IEnumerable<T> source, int pageSize, Func<IEnumerable<T>, IEnumerable<TResult>> func, List<TResult> results)
        {
            var totalPage = Math.Ceiling(source.Count() * 1.0 / pageSize);

            for (int i = 0; i < totalPage; i++)
            {
                var skip = i * pageSize;
                var items = source.Skip(skip).Take(pageSize);
                results.AddRange(func(items));
            }
        }

        public static List<TResult> BatchExecute<T, TResult>(this IEnumerable<T> source, int pageSize, Func<IEnumerable<T>, IEnumerable<TResult>> func)
        {
            List<TResult> results = new List<TResult>();
            source.BatchExecute(pageSize, func, results);
            return results;
        }
    }
}
