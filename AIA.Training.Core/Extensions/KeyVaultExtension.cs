using AIA.One.Core.Cryptography;
using AIA.Training.Core.Configs;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace AIA.Training.Core.Extensions
{
    public static class KeyVaultExtension
    {
        private static AzureKeyVault Vault;

        public static void InitVault(string client, string clientSecret, string name)
        {
            KeyVaultExtension.Vault = new AzureKeyVault(client, clientSecret, name);
        }

        public static void ApplyVault(this AppSettings appSetting)
        {
            if (appSetting == null)
                throw new NullReferenceException("Appsettings null");
            if (KeyVaultExtension.Vault == null)
                throw new NullReferenceException("Need to call InitVault before apply vault to setting");
            KeyVaultExtension.ApplyKeyVaultProperties("AppSettings", appSetting.GetType(), (object)appSetting);
        }

        public static void ApplyVault(this JwtSettings jwtSettings)
        {
            if (jwtSettings == null)
                throw new NullReferenceException("Jwtsettings null");
            if (KeyVaultExtension.Vault == null)
                throw new NullReferenceException("Need to call InitVault before apply vault to setting");
            KeyVaultExtension.ApplyKeyVaultProperties("JwtSettings", jwtSettings.GetType(), (object)jwtSettings);
        }

        private static void ApplyKeyVaultProperties(string prefix, Type type, object instance)
        {
            List<PropertyInfo> list = ((IEnumerable<PropertyInfo>)type.GetProperties(BindingFlags.Instance | BindingFlags.Public)).ToList<PropertyInfo>();
            for (int index = 0; index < list.Count; index++)
            {
                Type propertyType = list[index].PropertyType;
                if (!propertyType.IsArray && list[index].GetCustomAttributes<KeyVaultAttribute>().Any<KeyVaultAttribute>())
                {
                    if(instance != null)
                    {
                        var key = prefix + "-" + list[index].Name;
                        Console.WriteLine(key);
                        try
                        {
                            var vaultValue = (object)KeyVaultExtension.Vault.GetKey(key).GetAwaiter().GetResult();
                            if (vaultValue != null)
                            {
                                list[index].SetValue(instance, vaultValue);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception($"get {key} from keyvault error", ex);
                        }                       
                    }
                }
                if (propertyType.IsClass && !propertyType.FullName.StartsWith("System."))
                    KeyVaultExtension.ApplyKeyVaultProperties(prefix + "-" + list[index].Name, propertyType, list[index].GetValue(instance));
            }
        }
    }
}
