using AIA.One.Core.Jobs;

namespace AIA.Training.Core.Extensions
{
    public static class BackgroundJobExtension
    {
        public static void Delete(this IBackgroundJob background, string jobId)
        {
            Hangfire.BackgroundJob.Delete(jobId);
        }
    }
}