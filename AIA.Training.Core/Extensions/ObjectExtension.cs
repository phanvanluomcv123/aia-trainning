using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIA.Training.Core.Extensions
{
    public static class ObjectExtension
    {
        public static string Serialize(this object obj)
        {
            if (obj == null)
                return string.Empty;

            return JsonConvert.SerializeObject(obj);
        }

        public static void ConvertDateTimeFieldsToVNTime(this object obj)
        {
            foreach (var propertyInfo in obj.GetType().GetProperties())
            {
                if (propertyInfo.PropertyType == typeof(DateTime) || propertyInfo.PropertyType == typeof(DateTime?))
                {
                    var dt = (DateTime?)propertyInfo.GetValue(obj);
                    if (dt.HasValue)
                    {
                        propertyInfo.SetValue(obj, dt.Value.AddHours(7));
                    }
                }
            }
        }
    }
}
