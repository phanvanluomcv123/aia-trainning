﻿using System;
using System.Collections.Generic;
using System.Text;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Resources;
using FluentValidation;

namespace AIA.Training.Validators
{
    public partial class DeleteRoomValidator
    {
        private readonly IRoomBusiness _roomBusiness;

        public DeleteRoomValidator(IRoomBusiness roomBusiness) : this()
        {
            _roomBusiness = roomBusiness;
        }

        protected override void CustomRules()
        {
            RuleFor(r => r.Id).Custom((roomId, context) =>
            {
                var room = _roomBusiness.GetById(roomId);
                if (room == null)
                {
                    context.AddFailure(nameof(DeleteRoom.Id), ErrorResource.NotExistRoomId);
                }
            });
        }
    }
}
