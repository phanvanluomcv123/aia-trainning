﻿using AIA.One.Core.CQRS;
using AIA.Training.Core.CQRS.Commands;
using FluentValidation;
using System;

namespace AIA.Training.Validators
{
    public partial class CreateOrUpdateCategoryValidator: BaseCommandValidator<CreateOrUpdateCategory>
    {
        public CreateOrUpdateCategoryValidator()
        {
			RuleFor(c => c.Name).NotNull();
			CustomRules();
        }
    }
    public partial class CreateOrUpdateRoomValidator: BaseCommandValidator<CreateOrUpdateRoom>
    {
        public CreateOrUpdateRoomValidator()
        {
			RuleFor(c => c.RoomNumber).NotNull();
			RuleFor(c => c.Location).NotNull();
			CustomRules();
        }
    }
    public partial class DeleteRoomValidator: BaseCommandValidator<DeleteRoom>
    {
        public DeleteRoomValidator()
        {
			RuleFor(c => c.Id).NotNull();
			CustomRules();
        }
    }
}
