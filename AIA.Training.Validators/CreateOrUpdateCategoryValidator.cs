using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Resources;
using FluentValidation;
using System;
using System.Text.RegularExpressions;
using AIA.Training.Core.Configs;

namespace AIA.Training.Validators
{
    public partial class CreateOrUpdateCategoryValidator
    {
        private readonly ICategoryBusiness _categoryBusiness;

        public CreateOrUpdateCategoryValidator(ICategoryBusiness categoryBusiness)
        {
            _categoryBusiness = categoryBusiness;
        }

        protected override void CustomRules()
        {

            RuleFor(c => c.Name).MaximumLength(250);

            
            RuleFor(c => c.Id).Custom((categoryId, context) =>
            {
                var category = _categoryBusiness.GetById(categoryId);
                if (category == null)
                {
                    context.AddFailure(nameof(CreateOrUpdateCategory.Id), ErrorResource.InvalidCourseCategoryId);
                }
            });
        }
    }
}