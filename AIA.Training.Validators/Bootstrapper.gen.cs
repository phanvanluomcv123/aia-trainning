﻿using AIA.One.Core.CQRS;
using AIA.One.Core.DependencyInjection;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Validators;

namespace AIA.Training.Validators
{
	public class Bootstrapper : IWireUpDependencies
	{
		private readonly IRegisterDependencies container;

        public Bootstrapper(IRegisterDependencies container)
        {
            this.container = container;
        }

		public void WireUp()
        {
			container.Register<ICommandValidator<CreateOrUpdateCategory>, CreateOrUpdateCategoryValidator>();
			container.Register<ICommandValidator<CreateOrUpdateRoom>, CreateOrUpdateRoomValidator>();
			container.Register<ICommandValidator<DeleteRoom>, DeleteRoomValidator>();
        }
	}
}
