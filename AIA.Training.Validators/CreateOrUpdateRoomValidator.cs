﻿using System;
using System.Collections.Generic;
using System.Text;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.CQRS.Commands;
using AIA.Training.Resources;
using FluentValidation;

namespace AIA.Training.Validators
{
    public partial class CreateOrUpdateRoomValidator
    {
        private readonly IRoomBusiness _roomBusiness;

        public CreateOrUpdateRoomValidator(IRoomBusiness roomBusiness)
        : this()
        {
            _roomBusiness = roomBusiness;
        }

        protected override void CustomRules()
        {
            RuleFor(r => r.Id).Custom((roomId, context) =>
            {
                var entity = _roomBusiness.GetById(roomId);
                if (entity != null)
                {
                    context.AddFailure(nameof(CreateOrUpdateRoom.Id), ErrorResource.ExistRoomId);
                }
            });
        }
    }
}
