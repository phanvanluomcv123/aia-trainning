using AIA.One.Core.Messages;
using AIA.Training.Core.Configs;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIA.Training.Infrastructure.Messages
{
    public static class MessageExtensions
    {
        public static void AddEventGrid(this IServiceCollection services)
        {
            services.AddSingleton<IEventGridConfiguration, EventGridConfiguration>();
            services.AddSingleton<IEventGridPublisher, EventGridPublisher>();

        }

        public static void AddEventHub(this IServiceCollection services)
        {
            var hubConnection = AppSettings.Instance.EventHub.HubConnection;
            var hubName = AppSettings.Instance.EventHub.HubNamespace;
            var azureEventHubSubscriber = new AzureEventHubSubcriber(hubConnection, hubName);
            services.AddSingleton<IEventHubSubcriber>(azureEventHubSubscriber);
        }
    }
}
