using AIA.Training.Core.Configs;

namespace AIA.Training.Infrastructure.Messages
{
    public interface IEventGridConfiguration 
    {
        EventGridSetting EventGrid { get; set; }
    }
}
