using AIA.Training.Core.Configs;
using Microsoft.Extensions.Options;

namespace AIA.Training.Infrastructure.Messages
{
    public class EventGridConfiguration : IEventGridConfiguration
    {
        public EventGridSetting EventGrid { get; set; }

        public EventGridConfiguration(AppSettings settings)
        {
            this.EventGrid = settings.EventGrid;
        }
    }
}
