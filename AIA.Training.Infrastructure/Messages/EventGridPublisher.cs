using AIA.One.Core.Messages;
using AIA.Training.Core.Configs;
using Microsoft.Azure.EventGrid;
using Microsoft.Azure.EventGrid.Models;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AIA.Training.Infrastructure.Messages
{
    public class EventGridPublisher : AzureEventGridPublisher
    {
        public EventGridPublisher(
            ILogger logger,
            IEventGridConfiguration eventGridConfiguration) : base(eventGridConfiguration.EventGrid.TopicEndpoint, eventGridConfiguration.EventGrid.TopicKey)
        {
        }

    }
}
