using AIA.One.Core.Caching;
using AIA.Training.Core.Configs;
using System;
using System.Collections.Generic;
using System.Text;

namespace AIA.Training.Infrastructure.Caching
{
    public class OneMobileDefaultCacheKeyService : DefaultCacheKeyService, ICacheKeyService
    {
        private readonly string _applicationCacheKeyPrefix;

        public OneMobileDefaultCacheKeyService(AppSettings appSettings, CachingSettings cachingSettings) : base(cachingSettings)
        {
            //_applicationCacheKeyPrefix = appSettings.Redis.AppCacheKeyPrefix;
        }

        public override CacheKey PrepareKey(CacheKey cacheKey, params object[] keyObjects)
        {            
            return base.PrepareKey(PrepareKeyForAppCacheKeyPrefix(cacheKey), keyObjects);
        }

        public override CacheKey PrepareKeyForDefaultCache(CacheKey cacheKey, params object[] keyObjects)
        {
            return base.PrepareKeyForDefaultCache(PrepareKeyForAppCacheKeyPrefix(cacheKey), keyObjects);
        }

        public override CacheKey PrepareKeyForShortTermCache(CacheKey cacheKey, params object[] keyObjects)
        {
            return base.PrepareKeyForShortTermCache(PrepareKeyForAppCacheKeyPrefix(cacheKey), keyObjects);
        }

        private CacheKey PrepareKeyForAppCacheKeyPrefix(CacheKey cacheKey)
        {
            if (!string.IsNullOrEmpty(_applicationCacheKeyPrefix) && !cacheKey.Key.Contains(_applicationCacheKeyPrefix))
            {
                string keyAppendAppPrefix = $"{_applicationCacheKeyPrefix}.{cacheKey.Key}";

                return new CacheKey(keyAppendAppPrefix, cacheKey.Prefixes.ToArray())
                {
                    CacheTime = cacheKey.CacheTime
                };
            }

            return cacheKey;
        }
    }
}
