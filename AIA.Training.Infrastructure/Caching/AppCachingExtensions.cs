using AIA.One.Core.Caching;
using AIA.One.Core.Caching.Redis;
using AIA.One.Core.Configuration;
using AIA.Training.Core.Configs;
using Microsoft.Extensions.DependencyInjection;

namespace AIA.Training.Infrastructure.Caching
{
    public static class AppCachingExtensions
    {
        public static void AddAppCachingWithMemoryOrRedis(this IServiceCollection services, AppSettings appSettings)
        {
            // Static cache manager & redis connection wrapper
            //services.AddSingleton(new AppStartupConfig
            //{
            //    RedisEnabled = appSettings.Redis.RedisEnabled,
            //    UseRedisForCaching = appSettings.Redis.UseRedisForCaching,
            //    IgnoreRedisTimeoutException = appSettings.Redis.IgnoreRedisTimeoutException,
            //    RedisConnectionString = appSettings.Redis.RedisConnectionString,
            //    RedisDatabaseId = appSettings.Redis.RedisDatabaseId.GetValueOrDefault(0)
            //});
            //services.AddSingleton(new CachingSettings
            //{
            //    DefaultCacheTime = appSettings.Redis.DefaultCacheTimeInMinute,
            //    ShortTermCacheTime = appSettings.Redis.ShortTermCacheTimeInMinute
            //});

            //if (appSettings.Redis.RedisEnabled && appSettings.Redis.UseRedisForCaching)
            //{
            //    services.AddSingleton<ICacheLocker, RedisConnectionWithRedLockWrapper>();
            //    services.AddSingleton<IRedisConnectionWrapper, RedisConnectionWithRedLockWrapper>();
            //    services.AddScoped<IStaticCacheManager, RedisCacheManager>();
            //}
            //else
            //{
            //    services.AddSingleton<ICacheLocker, MemoryCacheManager>();
            //    services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            //}
            //services.AddScoped<ICacheKeyService, DefaultCacheKeyService>();
            services.AddSingleton<ICacheLocker, MemoryCacheManager>();
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            services.AddScoped<ICacheKeyService, OneMobileDefaultCacheKeyService>();
        }
    }
}
