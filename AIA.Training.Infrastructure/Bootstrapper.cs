using AIA.One.Core.DependencyInjection;
using AIA.Training.Core.DependencyInjection;

namespace AIA.Training.Infrastructure
{
    public class Bootstrapper : BaseWireUpDependencies
    {
        public Bootstrapper(IRegisterDependencies registerDependencies) : base(registerDependencies)
        {
        }

        public override void WireUp()
        {
        }
    }
}
