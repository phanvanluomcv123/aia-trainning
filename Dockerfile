FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env

ADD ./AIA.One.MobileManagement.Services /build/AIA.One.MobileManagement.Services
ADD ./lib /build/lib
ADD ./NuGet.Config /build
ADD ./AIA.One.MobileManagement.Core /build/AIA.One.MobileManagement.Core
ADD ./AIA.One.MobileManagement.Business /build/AIA.One.MobileManagement.Business
ADD ./AIA.One.MobileManagement.CommandHandler /build/AIA.One.MobileManagement.CommandHandler
ADD ./AIA.One.MobileManagement.Data /build/AIA.One.MobileManagement.Data
ADD ./AIA.One.MobileManagement.EventHandler /build/AIA.One.MobileManagement.EventHandler
ADD ./AIA.One.MobileManagement.Validators /build/AIA.One.MobileManagement.Validators
ADD ./AIA.One.MobileManagement.Resources /build/AIA.One.MobileManagement.Resources

WORKDIR /build
RUN dotnet restore AIA.One.MobileManagement.Services/AIA.One.MobileManagement.Services.csproj
RUN dotnet build 'AIA.One.MobileManagement.Services/AIA.One.MobileManagement.Services.csproj' -c Release -o /app

WORKDIR /app
ENTRYPOINT ["dotnet", "AIA.One.MobileManagement.Services.dll"]