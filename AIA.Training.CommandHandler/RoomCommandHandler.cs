﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AIA.One.Core.CQRS;
using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.CQRS.Commands;
using Kledex.Commands;
using Kledex.Events;

namespace AIA.Training.CommandHandler
{
    public class RoomCommandHandler : ICommandHandlerAsync<CreateOrUpdateRoom>, ICommandHandlerAsync<DeleteRoom>
    {
        private readonly IRoomBusiness _roomBusiness;
        private readonly ICommandValidator<CreateOrUpdateRoom> _commandValidator;
        private readonly ICommandValidator<DeleteRoom> _deleteValidator;

        public RoomCommandHandler(IRoomBusiness roomBusiness, ICommandValidator<CreateOrUpdateRoom> commandValidator, ICommandValidator<DeleteRoom> deleteValidator)
        {
            _roomBusiness = roomBusiness;
            _commandValidator = commandValidator;
            _deleteValidator = deleteValidator;
        }

        public async Task<CommandResponse> HandleAsync(CreateOrUpdateRoom command)
        {
            _commandValidator.ValidateCommand(command);
            await _roomBusiness.CreateOrUpdate(command);
            return new CommandResponse()
            {
                Events = new List<IEvent>()
            };
        }

        public async Task<CommandResponse> HandleAsync(DeleteRoom command)
        {
            _deleteValidator.ValidateCommand(command);
            await _roomBusiness.DeleteRoom(command);
            return new CommandResponse()
            {
                Events = new List<IEvent>()
            };
        }
    }
}
