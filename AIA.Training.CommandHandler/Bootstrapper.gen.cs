﻿using AIA.One.Core.DependencyInjection;
using AIA.Training.Core.CQRS.Commands;
using System;
using System.Collections.Generic;

namespace AIA.Training.CommandHandler
{
	public class Bootstrapper : IWireUpDependencies
	{
		private readonly IRegisterDependencies container;

        public Bootstrapper(IRegisterDependencies container)
        {
            this.container = container;
        }

		public void WireUp()
        {
			
			// register handler
			//container.Register(typeof(CreateOrUpdateCategoryHandler));
			//container.Register(typeof(CreateOrUpdateRoomHandler));
			//container.Register(typeof(DeleteRoomHandler));
			
			// register command handler
			//commandBus.RegisterCommandHandler<CreateOrUpdateCategory, CreateOrUpdateCategoryHandler>();
			//commandBus.RegisterCommandHandler<CreateOrUpdateRoom, CreateOrUpdateRoomHandler>();
			//commandBus.RegisterCommandHandler<DeleteRoom, DeleteRoomHandler>();
        }
	}
}
