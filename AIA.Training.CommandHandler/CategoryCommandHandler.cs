using AIA.Training.Core.Contracts.Business;
using AIA.Training.Core.CQRS.Commands;
using Kledex.Commands;
using Kledex.Events;
using System.Collections.Generic;
using System.Threading.Tasks;
using AIA.One.Core.CQRS;

namespace AIA.Training.CommandHandler
{
    public class CategoryCommandHandler : ICommandHandlerAsync<CreateOrUpdateCategory>
    {
        private readonly ICategoryBusiness _business;
        private readonly ICommandValidator<CreateOrUpdateCategory> _createOrUpdateValidator;

        public CategoryCommandHandler(ICategoryBusiness business, ICommandValidator<CreateOrUpdateCategory> createOrUpdateValidator)
        {
            _business = business;
            _createOrUpdateValidator = createOrUpdateValidator;
        }

        public async Task<CommandResponse> HandleAsync(CreateOrUpdateCategory command)
        {
            _createOrUpdateValidator.ValidateCommand(command);

            await _business.CreateOrUpdate(command);

            return await Task.FromResult(new CommandResponse { Events = new List<IEvent>() });
        }
    }
}
